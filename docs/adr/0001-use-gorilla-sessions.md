# Use gorilla/sessions for session management

## Context and Problem Statement

We need a library to manage the sessions for the SystemTestPortal. Which library can be used for this?

## Considered Options

* gorilla/sessions

## Decision Outcome

Chosen option: "gorilla/sessions", because it's easy to use and popular.
