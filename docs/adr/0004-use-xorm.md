# Use xorm as ORM

* Status: accepted
* Deciders: @SilentTeaCup, @m.haug, @kuleszdl
* Date: 2017-12-01

Technical Story: #109

## Context and Problem Statement

Originally all data was stored in maps in memory. This meant all data was lost when the server was restarted. To enable
persistent storage of data, we decided to use a relational database. Since we did not want to limit us to one database
system, we decided to use an ORM to simplify supporting multiple database systems.

## Decision Drivers

* Is the ORM in use in other projects?
* Is the API easy to use and well documented?
* Does the ORM support SQLite and PostgreSQL?

## Considered Options

* [xorm](https://github.com/go-xorm/xorm)
* [gorm](https://github.com/jinzhu/gorm)
* [db](https://github.com/upper/db)

## Decision Outcome

Chosen option: "xorm", because it was the only option that had widespread use.
