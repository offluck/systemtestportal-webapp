# Use httptreemux as router

* Status: accepted
* Deciders: @m.haug, @SilentTeaCup
* Date: 2017-12-12

Technical Story:  
In #29 it was proposed that a routing "framework" should be used to improve the
readability and maintainability of the handlers. We first settled on 
[gin](https://github.com/gin-gonic/gin) which we found could not be used since it 
didn't allow the combination of static and dynamic routes. Therefore we transitioned
to [httptreemux](https://github.com/dimfeld/httptreemux).
Development started with #148.

## Context and Problem Statement

The manual coded routing in our project was non-transparent and confusing to 
deal with. Therefore we decided we wanted to streamline routing using a 
library/framework. Which option for routing fits our project best and supports
our wishes for the URL-scheme.

## Considered Options

* [gin](https://github.com/gin-gonic/gin)
* [httptreemux](https://github.com/dimfeld/httptreemux)
* [beego](https://github.com/astaxie/beego)
* [web](https://github.com/gocraft/web)
* [goji](https://github.com/zenazn/goji/)
* [mux](http://www.gorillatoolkit.org/pkg/mux)
* [httprouter](https://github.com/julienschmidt/httprouter)
* [martini](https://github.com/go-martini/martini)

## Decision Outcome

Chosen option: "[httptreemux](https://github.com/dimfeld/httptreemux)", because 
it supports the combination of dynamic and static routes and is relatively 
lightweight and less intrusive (support classical handlers to minimize the amount
of code that needs to be rewritten when the router is changed) than other options.
