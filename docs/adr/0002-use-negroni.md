# Use negroni as middleware

* Status: accepted
* Deciders: @m.haug, @SilentTeaCup
* Date: 2017-12-19

## Context and Problem Statement

We needed a comprehensive solution which would allow us to easily deploy 
middleware. 

## Considered Options

* [negroni](https://github.com/urfave/negroni)
* [claw](https://github.com/go-zoo/claw)

## Decision Outcome

Chosen option: "[negroni](https://github.com/urfave/negroni)", because it
support the standard handlers from the golang http library and seemed to be the
easiest one to use.
