/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

let historySTP = [];
let oldURL = window.location.pathname;
let pressedBack = false;

/**
 * Returns the history of visited sites of STP
 *
 * @returns the history of visited sites of STP
 */
function getHistorySTP() {
    return historySTP;
}

/**
 * Pops the stack of the page history of STP
 *
 * @returns last visited page
 */
function popHistorySTP() {
    return getHistorySTP().pop();
}

/**
 * Stores the last visited page of STP, except the back button was pressed.
 */
function pushHistory() {
    if (window.location.pathname !== oldURL) {
        if (!pressedBack) {
            historySTP.push(oldURL);

        }
        oldURL = window.location.pathname;
        pressedBack = false;
    }
}

/**
 * Back Button functionality to go back to previous site
 */
function backButton(event, expectedBehaviour) {
    pressedBack = true;
    if(getHistorySTP().length > 0) {
        ajaxRequestFragment(event, buildURL(popHistorySTP()).toString(), "", "GET");
    } else {
        expectedBehaviour(event);
    }
}
