{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
<div class="row searchbar">
    <div class="col-12">
        <form role="search" class="search-form">
            <label class="sr-only">{{ T "Search" .}}</label>
            <div class="input-group flex-nowrap">
                <input id="users-search-input" type="search" name="s" class="search-field form-control float-left rounded filter-search" placeholder="Search...">
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="m-4">

        <table id="users-list" class="table w-100" style="opacity: 0">
            <thead>
            <tr>
                <th class="pl-0 pt-0" style="border-top: none;">
                    <h5>{{ T "Users (with public profiles)" .}}</h5>
                </th>
            </tr>
            </thead>
            <tbody>
                {{ range .Users }}
                    {{if .IsShownPublic}}
                    <tr class="userLine" id="{{ .Name }}">
                        <td class="pl-0 pr-0">
                            <div class="d-flex flex-row">
                                <div class="mr-3 d-none d-sm-block">
                                    <img src="{{getImagePath .Image "user"}}" class="rounded profile-picture" alt="user image" width="115" height="115">
                                </div>
                                <div class="card-block" style="flex: 1;">
                                    <span class="d-none d-sm-block float-right badge badge-secondary">{{ .Name }}</span>
                                    <h4 class="card-title">{{ .DisplayName }}</h4>
                                    {{if .IsEmailPublic}}<p class="card-text font-italic filter-by-search">{{ .Email }}</p>{{end}}
                                </div>
                            </div>
                        </td>
                    </tr>
                    {{end}}
                {{ else }}

                    <tr>
                        <td>

                            <!-- Placeholder -->
                            <p class="text-center empty-group-list">
                                <span>{{ T "This list contains all visible users. Currently, there are no users." .}}</span><br/><br/>
                            </p>

                        </td>
                    </tr>

                {{ end }}
            </tbody>
        </table>

    </div>
</div>
<script src="/static/js/explore/explore.js" integrity="{{sha256 "/static/js/explore/explore.js"}}"></script>
<script type='text/javascript'>

    $(".userLine").on("click", event => useUserLink(event));

    $(() => {
        $("#menuExploreUsers").addClass("disabled active");
        $("#navbarExploreUsers").addClass("disabled active");

        const idTable = "users-list";
        const idSearch = "users-search-input";

        const listElementsPerPage = 10;
        const listElements = "users";

        initializeDataTable(idTable, idSearch, listElementsPerPage, listElements)
    });
</script>
{{end}}


