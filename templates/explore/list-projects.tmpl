{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Case List" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            Shows all projects of this instance of Systemtestportal
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="order-3 btn btn-primary align-middle">
                                + <span class="d-none d-sm-inline">New Project</span>
                            </button>
                        </td>
                        <td>Create a new project</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>Plus</button></td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<div class="row searchbar">
	<div class="col-12">
		<form role="search" class="search-form">
			<label class="sr-only">{{T "Search" .}}</label>
			<div class="input-group flex-nowrap">
				<input id="projects-search-input" type="search" name="s" class="order-1 search-field form-control float-left rounded" placeholder="{{T "Search..." .}}">
				{{ if .SignedIn }}
                    <a id="buttonNewProject" class="order-3 btn btn-primary align-middle ml-4 float-right" href="/projects/new">
                        + <span class="d-none d-sm-inline">New Project</span>
                    </a>
                    <span id="helpIcon" class="ml-auto float-right order-4">
                        <a class="nav-link disabled No-Warning-On-Current-Action-Abort" id="tabButtonHelp" data-toggle="modal"
                           data-target="#helpModal" href="" role="tab">
                            <i class="fa fa-question-circle" aria-hidden="true" data-toggle = "tooltip" data-placement = "bottom" data-original-title="Help"></i>
                        </a>
                    </span>
                {{ end }}
			</div>
		</form>
	</div>
</div>
<div class="card">
	<div class="m-4">

		<table id="projects-list" class="table w-100" style="opacity: 0">
            <thead>
                <tr>
                    <th class="pl-0 pt-0" style="border-top: none;">
                        <h5>
                        {{ T "Projects" .}} {{ if .SignedIn }}{{ T "(public, internal, private)" .}}{{ else }}{{ T "(public)" .}}{{ end }}
                        </h5>
                    </th>
                </tr>
            </thead>
            <tbody>
                {{ range .Projects }}

                    <tr class="projectLine" id="{{ .Name }}" title="{{ .Owner }}">
                        <td class="pl-0 pr-0">
                            <div class="d-flex flex-row">
                                <div class="mr-3 d-none d-sm-block">
                                    <img src="{{getImagePath .Image "project" }}" class="rounded" alt="project logo" width="115" height="115">
                                </div>
                                <div class="card-block" style="flex: 1;">
                                    <span class="d-none d-sm-block float-right badge badge-secondary"><span>{{T "created" .}} </span><time class="timeago" datetime="{{ provideTimeago .CreationDate }}"></time></span>
                                    <h4 class="card-title filter-by-search">{{ .Owner.Actor }}/{{ .Name }}</h4>
                                    <p class="card-text" >{{printMarkdown .Description }}</p>
                                </div>
                            </div>
                        </td>
                    </tr>

                {{ else }}

                    <tr>
                        <td>

                            <!-- Placeholder -->
                            <p class="text-center empty-project-list">
                                <span>{{ T "This list contains all visible projects. Currently, there are no projects." .}}</span><br/><br/>
                            {{ if .SignedIn }}
                                <a class="btn btn-primary align-middle" href="/projects/new">
                                    <span>{{ T "New Project" .}}</span>
                                </a>
                            {{ end }}
                            </p>

                        </td>
                    </tr>

                {{ end }}
            </tbody>
        </table>

	</div>
</div>

<script src="/static/js/explore/explore.js" integrity="{{sha256 "/static/js/explore/explore.js"}}"></script>
<script type='text/javascript'>

    $(".projectLine").on("click", event => useProjectLink(event));
    $("time.timeago").timeago();

    $(() => {
        $("#menuExploreProjects").addClass("disabled active");
        $("#navbarExploreProjects").addClass("disabled active");

        const idTable = "projects-list";
        const idSearch = "projects-search-input";

        const listElementsPerPage = 10;
        const listElements = "projects";

        initializeDataTable(idTable, idSearch, listElementsPerPage, listElements)
    });

    /**
     * Keyboard support
     */
    $(document)
            .off("keydown.event")
            .on("keydown.event", function (event) {
                if($('.modal .active').length > 0){
                    return;
                }

                switch (event.target.tagName) {
                    case "INPUT": case "SELECT": case "TEXTAREA": return;
                }
        let buttonString;
        switch (event.key) {
            case "+":
                buttonString = "buttonNewProject";
                break;
        }
        if(document.getElementById(buttonString) != null){
            document.getElementById(buttonString).click();
        }
    });
</script>
{{end}}

