{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}
{{define "modal-manage-labels"}}

<!-- Style Sheets -->
<link href="/static/css/jquery-hex-colorpicker.css" integrity="{{sha256 "/static/css/jquery-hex-colorpicker.css"}}" rel="stylesheet" type="text/css">

<!-- Top-Level-Modals-->
{{template "modal-delete-confirm" .DeleteLabels}}

<!------------------ Sub-Modals -------------------->
<!-- Input Error -->
<div class="modal fade" style="z-index: 9999" id="errorMessageModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Error" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="errorMessage">
                <p></p>
            </div>
            <div class="modal-footer">
                <button id="buttonClose" type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Unsaved Changes -->
<div class="modal fade" style="z-index: 9999" id="unsavedChangesModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Exiting with unsaved changes" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "Some changes are not saved yet. Continue?" .}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonApplyChangesClosing">{{T "Apply Changes" .}}</button>
                <button type="button" class="btn btn-danger" id="buttonDiscardChangesClosing">{{T "Discard Changes" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Abort" .}}</button>
            </div>
        </div>
    </div>
</div>

<!-- unsaved changes switching -->
<div class="modal fade" style="z-index: 9999" id="unsavedChangesModalSwitching">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Switching labels with unsaved changes" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "Some changes are not saved yet. Continue?" .}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonApplyChangesSwitching">{{T "Apply Changes" .}}</button>
                <button type="button" class="btn btn-danger" id="buttonDiscardChangesSwitching">{{T "Discard Changes" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Abort" .}}</button>
            </div>
        </div>
    </div>
</div>


<!-------------- Main-Modal ---------------->
<div class="modal fade" id="modal-manage-labels" tabindex="-1" role="dialog" aria-labelledby="modal-manage-labels-label"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 750px">
        <div class="modal-content">

            <!------------------------------------------------------------------------------------->
            <div class="modal-header">
                <h5 class="modal-title" id="modal-manage-labels-label">
                    <i class="fa fa-wrench" aria-hidden="true"></i> {{T "Manage Labels" .}}
                </h5>
                <button id="closeButtonTop" type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!------------------------------------------------------------------------------------->
            <div>
                <!------------------------------------------------------------------------------------->
                <div class="modal-body" style="width: 30%; float:left; padding-bottom: 0px;">
                    <button id="buttonNewLabel"
                            type="button"
                            class="btn label-list-btn"
                            onclick="onModalLabelClick('buttonNewLabel')"
                            style="opacity: 1; font-size: 1.25rem; margin-bottom: 5px;color: #fff;background-color: #5d6770;border-color: #5d6770;">New Label</button>

                    <h5>Labels</h5>

                    <div class="row" style="overflow-y: auto; max-height: 212px;">
                        <div id="modalLabelContainer" class="col-12">
                                {{range .Project.Labels}}
                                    <button class="btn label-list-btn"
                                            style=" background-color: {{.Color}};
                                                    color:{{.TextColor}};"
                                            id="modal-container-label-{{.Id}}"
                                            data-toggle="tooltip"
                                            data-original-title="{{ .Description }}"
                                            data-placement="left"
                                            onclick="onModalLabelClick({{.Id}})">{{ .Name }}</button>
                                {{end}}
                        </div>
                    </div>
                </div>
                <!------------------------------------------------------------------------------------->
                <div class="modal-body" style="width:70%; float:left;  border-left: 1px solid #e9ecef">
                    <h5 id="currentLabelText">{{T "New Label" .}}</h5>

                    <div class="row col-12">
                        <!------------------------------------------------------------------------------------->
                        <div class=" row col-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="labelNameText">{{T "Name" .}}</span>
                                </div>
                                <input id="inputLabelName"
                                       class="form-control"
                                       type="text"
                                       placeholder="e.g. GUI"
                                       aria-label="e.g. GUI"
                                       aria-describedby="labelNameText"
                                       pattern="[\sA-Za-z0-9-_().,:äöüÄÖÜß]{1,256}"
                                       required>
                            </div>
                        </div>
                        <!----------------------------------------------->
                        <div class="row col-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputDescriptionText">{{T "Description" .}}</span>
                                </div>

                                <input id="inputLabelDescription"
                                       class="form-control"
                                       type="text"
                                       placeholder="e.g. Tests for the Graphical User Interface"
                                       aria-label="e.g. Tests for the Graphical User Interface"
                                       aria-describedby="inputDescriptionText">
                            </div>
                        </div>
                        <!----------------------------------------------->

                        <div class="row col-12" >

                            <div class="input-group mb-3" style="border: 1px solid #ced4da;border-radius: .25rem;width:53%">
                                <div class="input-group-prepend">
                                    <span id="inputColorPicker" class="input-group-text" style="max-height: 2.5rem">{{T "Color" .}}</span>
                                </div>

                                <input id="inputLabelColor"
                                       type="button"
                                       value="Click to change"
                                       class="btn btn-primary"
                                       style="max-height: 2.5rem;border: 1px solid #ced4da;border-radius: .25rem;">
                            </div>
                        </div>

                        <div class="row col-12">
                            <div style="white-space:nowrap;overflow: auto;border: 1px solid #ced4da;border-radius: .25rem;">
                                <button class="btn" style="color: #495057;background-color: #e9ecef;border: 1px solid #ced4da;border-radius: .25rem;">
                                    {{T "Preview" .}}
                                </button>

                                <span id="labelPreview"
                                      class="badge badge-primary"
                                      style="max-width: 100%; white-space: normal;background-color: #007bff; margin-left: 1rem;"
                                      data-toggle="tooltip"
                                      data-original-title="">{{T "Preview" .}}</span>
                            </div>
                        </div>
                        <!------------------------------------------------------------------------------------->

                        <div class="col-12" style="margin-left: -1rem;margin-top:1rem">
                            <button id="buttonSaveLabel"
                                    class="btn btn-primary float-right"
                                    type="button">
                                {{T "Save" .}}
                            </button>

                            <button id="buttonDeleteLabel"
                                    class="btn btn-danger float-left"
                                    type="button"
                                    disabled data-toggle="modal"
                                    data-target="#deleteLabels">
                                {{T "Delete" .}}
                            </button>
                        </div>

                        <!------------------------------------------------------------------------------------->

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button id="closeButtonBottom" type="button" class="btn btn-secondary">
                    {{T "Close" .}}
                </button>
            </div>

        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/misc/label.js" integrity="{{sha256 "/static/js/misc/label.js"}}"></script>
<script src="/static/js/util/jquery-hex-colorpicker.js" integrity="{{sha256 "/static/js/util/jquery-hex-colorpicker.js"}}"></script>

<script>
    $("#inputLabelColor").hexColorPicker();
    $('#buttonSaveLabel').text('Add');
</script>

{{end}}