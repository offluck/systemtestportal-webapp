{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{/* This file defines the navigation bar in the projet settings */}}
{{define "settings-nav"}}

<div class="input-group flex-nowrap">

    <button type="button" class="btn btn-success" id="SaveProjectsSettingsButton" title="Save the changes made to the project settings" data-shortcut="S">
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
        <span class="d-none d-sm-inline">{{T "Save" .}}</span>
    </button>

    <button type="button" class="btn btn-primary ml-2" id="ExportJsonProjectButton" title="Export the project to import it in another instance of the Systemtestportal" data-shortcut="E">
        <i class="fa fa-file-text-o" aria-hidden="true"></i>
        <span class="d-none d-sm-inline">{{T "Export Project" .}}</span>
    </button>

    <button type="button" class="btn btn-danger ml-auto" id="ProjectDeleteButton"
            data-target="#deleteProject" data-toggle="modal" title="Opens a dialog to confirm to permanently delete the project" data-shortcut="Del">
        <i class="fa fa-trash-o" aria-hidden="true"></i>
        <span class="d-none d-sm-inline">{{T "Delete Project" .}}</span>
    </button>

</div>

<!-- Import Scripts here -->
<script>
    /**
     * Keyboard support
     */
    $(document)
            .off("keydown.event")
            .on("keydown.event", function (event) {
                if($('.modal .active').length > 0){
                    return;
                }

                switch (event.target.tagName) {
                    case "INPUT": case "SELECT": case "TEXTAREA": return;
                }
        let buttonString;
        switch (event.key) {
            case "E":
            case "e":
                buttonString = "ExportJsonProjectButton";
                break;
            case "Enter":
                buttonString = "SaveProjectsSettingsButton";
                break;
            case "Delete":
                buttonString = "ProjectDeleteButton";
                break;
        }
        if (document.getElementById(buttonString)) {
            document.getElementById(buttonString).click();
        }
    });
</script>
{{end}}