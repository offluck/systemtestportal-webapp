/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package integration

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

// GitLabIssue is the link between an issue in gitlab
// and the protocol in the stp.
type GitLabIssue struct {
	Protocol *test.CaseExecutionProtocol

	GitLabID int // The ID in gitlab
}

// NewIssue creates a new GitlabIssue that links a gitlab issue
// and a test protocol.
func NewIssue(protocol *test.CaseExecutionProtocol, issueID int) GitLabIssue {
	return GitLabIssue{
		Protocol: protocol,
		GitLabID: issueID,
	}
}
