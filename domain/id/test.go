/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

// TestExistenceChecker provides everything needed to check, if a test is already existent in storage.
type TestExistenceChecker interface {
	//Exists checks, if the given test is already existent in storage.
	Exists(id TestID) (bool, error)
}

//TestID hold all needed data to identify the a test in the whole system.
//This is the name, the superior project and a flag to differ between case and sequence
//Tests can be cases or sequences
type TestID struct {
	ProjectID
	test   string
	isCase bool
}

//NewTestID returns a new TestID containing the given name and superior project. No validation will be performed.
func NewTestID(project ProjectID, test string, testCase bool) TestID {
	return TestID{project, test, testCase}
}

//Test returns the name of the (superior) test
func (id TestID) Test() string {
	return id.test
}

//IsCase returns true if the test is a case and false if the test is a sequence
func (id TestID) IsCase() bool {
	return id.isCase
}

//Validate checks if this is a valid id for a new test.
func (id TestID) Validate(tec TestExistenceChecker) error {
	exists, err := tec.Exists(id)
	if err != nil {
		return err
	}
	if exists {
		return alreadyExistsErr(id)
	}
	return validateName(id.Test())
}
