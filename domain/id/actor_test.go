/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"testing"
)

func TestNewActorID(t *testing.T) {
	actorNames := []string{
		"testUser",
		"",
		"   ",
		"§&$/%",
		"hello-world",
	}
	for _, name := range actorNames {
		id := NewActorID(name)
		if name != id.Actor() {
			t.Errorf("New ActorID wasn't created correctly. Expected %q but got %q", name, id.Actor())
		}
	}
}

type ActorExistenceCheckerStub struct {
	exists bool
	err    error
}

func (ec ActorExistenceCheckerStub) Exists(ActorID) (bool, error) {
	return ec.exists, ec.err
}
func TestActorID_Validate(t *testing.T) {
	testData := []struct {
		name          string
		returnExists  bool
		returnError   error
		errorExpected bool
	}{
		{"testCaseName", false, nil, false},
		{"tre", false, nil, true},
		{"", false, nil, true},
		{"&$(=§$", false, nil, false},
		{"thisNameAlreadyExists", true, nil, true},
		{"name", false, fmt.Errorf("i'm an error"), true},
		{"This character is forbidden: /", false, nil, true},
		{"This character is forbidden: \\", false, nil, true},
		{"This character is forbidden: ?", false, nil, true},
		{"This character is forbidden: #", false, nil, true},
	}
	for _, set := range testData {
		id := NewActorID(set.name)
		result := id.Validate(ActorExistenceCheckerStub{set.returnExists, set.returnError})
		if set.errorExpected && (result == nil) {
			t.Errorf("Validate returned no error, but expected one. \nID: %+v", id)
		}
		if !set.errorExpected && (result != nil) {
			t.Errorf("Validate returned an error, but expected none. \nID: %+v \nError: %v", id, result)
		}
	}
}
