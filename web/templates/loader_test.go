/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package templates

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"
)

/*
	Temporary files
*/

// headerTmpl is an file temporally generated for this test,
// simulates a simple header template.
var headerTmpl = NewTmplFile(
	"header_tmpl.tmpl",
	"<html lang=\"en\">"+
		"	<head>"+
		"	</head>"+
		"	<body>"+
		"		{{template \"content\" . }}"+
		"		{{define \"nested\"}}test{{end}}"+
		"	</body>"+
		"</html>")

// contentTmpl is an file temporally generated for this test,
// simulates a simple content template.
var contentTmpl = NewTmplFile(
	"content_tmpl.tmpl",
	"{{define \"content\"}}"+
		"	ABC"+
		"{{end}}")

// TempFile represents a file that is temporally created for
// testing purposes.
type TempFile struct {
	name    string
	folder  string
	content string
}

// SimpleName gets the name of a file without the extensions.
func (t TempFile) SimpleName() string {
	return withoutExtension(t.name)
}

// Path gets the full path to the file.
func (t TempFile) Path() string {
	return filepath.Join(t.folder, t.name)
}

// NewTmplFile create a new TempFile configured to behave as a template file.
func NewTmplFile(name, content string) TempFile {
	folder, _ := filepath.Abs(filepath.Clean("../../templates"))
	return TempFile{name, folder, content}
}

// withoutExtension removes the extension of a files path.
func withoutExtension(file string) string {
	return file[:strings.LastIndex(file, ".")]
}

// createTmpFiles writes the given temporary files to the disk and
// returns the paths to all written files.
func createTmpFiles(files ...TempFile) []string {
	created := make([]string, len(files))
	for i, f := range files {
		created[i] = f.Path()
		writeFile(f.Path(), []byte(f.content))
	}
	return created
}

// writeFile writes a files with given name and content.
// If it fails it panics.
func writeFile(file string, content []byte) {
	if err := ioutil.WriteFile(file, content, 0644); err != nil {
		panic(err)
	}
}

// cleanUp tries to delete given files from the disk.
// If it fails it panics.
func cleanUp(files ...string) {
	ClearCache()
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			panic(err)
		}
	}
}

/*
	Tests
*/

func TestGet(t *testing.T) {
	defer cleanUp(createTmpFiles(contentTmpl, headerTmpl)...)
	exp := []string{"content", contentTmpl.name, headerTmpl.name, "nested"}

	ptmpl := Get(contentTmpl.SimpleName(), headerTmpl.SimpleName())
	var names []string
	for _, tmpl := range ptmpl.Templates() {
		names = append(names, tmpl.Name())
	}
	sort.Strings(names)
	for i, want := range exp {
		if names[i] != want {
			t.Errorf("Parsed files don't match "+
				"expectation: %s, want %s", names[i], want)
		}
	}
}

func TestAppend(t *testing.T) {
	defer cleanUp(createTmpFiles(headerTmpl, contentTmpl)...)
	tmpl := Append(nil, headerTmpl.SimpleName(), contentTmpl.SimpleName())
	checkForNilAndError(tmpl, nil, t)
	if tmpl.Lookup(headerTmpl.name) == nil {
		t.Error("Lookup of known to exist template did yield nil!")
	}

	tmpl = Append(nil)
	checkForNilAndError(tmpl, nil, t)

	base, _ := template.New("").Parse(contentTmpl.content)
	tmpl = Append(base, headerTmpl.SimpleName())
	checkForNilAndError(tmpl, nil, t)
	if tmpl.Lookup(contentTmpl.name) == nil {
		t.Error("Lookup of known to exist template did yield nil!")
	}

	tmpl = Append(base)
	checkForNilAndError(tmpl, nil, t)
	if tmpl != base {
		t.Errorf("The returned base is no the same anymore! "+
			"%+v and %+v are different!", tmpl, base)
	}
}

func TestUncache(t *testing.T) {
	defer cleanUp(createTmpFiles(headerTmpl, contentTmpl)...)

	ct := Get(contentTmpl.SimpleName())
	ht := Get(headerTmpl.SimpleName())
	writeFile(contentTmpl.Path(), []byte(""))
	Uncache(contentTmpl.SimpleName())

	nct := Get(contentTmpl.SimpleName())
	nht := Get(headerTmpl.SimpleName())

	if reflect.DeepEqual(ct, nct) {
		t.Errorf("Files stayed the same even after rewrite and cleared cache: "+
			"%v and are the same %v", ct, nct)
	}
	if ht != nht {
		t.Errorf("Template not cached: "+
			"%v and are not the same %v", ht, nht)
	}
}
func TestClearCache(t *testing.T) {
	defer cleanUp(createTmpFiles(headerTmpl, contentTmpl)...)

	oldTmpl := Get(headerTmpl.SimpleName(), contentTmpl.SimpleName()).
		Lookup("content")
	writeFile(contentTmpl.Path(), []byte(""))
	ClearCache()

	newTmpl := Get(headerTmpl.SimpleName(), contentTmpl.SimpleName()).
		Lookup("content")

	if reflect.DeepEqual(newTmpl, oldTmpl) {
		t.Errorf("Files stayed the same even after rewrite and cleared cache: "+
			"%v and are the same %v", oldTmpl, newTmpl)
	}
}

func TestLoad(t *testing.T) {
	defer cleanUp(createTmpFiles(contentTmpl)...)
	lt := Load(contentTmpl.SimpleName())
	tmpl := lt.Lookup("content").Get()
	checkForNilAndError(tmpl, nil, t)
}

func TestLoadedTemplateImpl_Append(t *testing.T) {
	file := NewTmplFile(
		"file_tmpl.tmpl",
		"{{define \"content\"}}"+
			"	ABCDEF"+
			"{{end}}")
	defer cleanUp(createTmpFiles(headerTmpl, contentTmpl, file)...)
	lt := Load(headerTmpl.SimpleName()).
		Funcs(template.FuncMap{"Test": func() string { return "CBA" }}).
		Append(contentTmpl.SimpleName()).
		Append(file.SimpleName()).
		Append()
	tmpl := lt.Lookup("content").Get()
	checkForNilAndError(tmpl, nil, t)
	tmpl = lt.Lookup(file.name).Get()
	checkForNilAndError(tmpl, nil, t)
}

func TestLoadedTemplateImpl_Funcs(t *testing.T) {
	file := NewTmplFile(
		"content_tmpl.tmpl",
		"{{define \"content\"}}"+
			"	ABC:{{ Test }}"+
			"{{end}}")
	defer cleanUp(createTmpFiles(file)...)
	w := bytes.NewBufferString("")

	Load().
		Funcs(template.FuncMap{"Test": func() string { return "CBA" }}).
		Append(file.SimpleName()).Get()

	tl := Load().
		Funcs(template.FuncMap{"Test": func() string { return "ABC" }}).
		Append(file.SimpleName()).Get()

	err := tl.Lookup("content").Execute(w, "lol")
	if err != nil {
		t.Errorf("Execution of template "+
			"failed because %s", err)
	}

	result := strings.TrimSpace(w.String())

	if result != "ABC:ABC" {
		t.Errorf("Function wasn't executed correctly. "+
			"Expexted %s but got %s.", "ABC:ABC", result)
	}
}

func TestLoadedTemplateImpl_Lookup(t *testing.T) {
	defer cleanUp(createTmpFiles(contentTmpl)...)
	if Load(contentTmpl.SimpleName()).Lookup(contentTmpl.name) == nil {
		t.Error("Lookup didn't find a template that " +
			"should be there and returned nil!")
	}
	if Load().Lookup("doesn't exist") != nil {
		t.Error("Lookup did find a template that shouldn't exist " +
			"and returned something diffrent to nil!")
	}
}

func TestLoadedTemplateImpl_Clone(t *testing.T) {
	file := NewTmplFile(
		"bla_tmpl.tmpl",
		"{{define \"bla\"}}{{end}}")
	defer cleanUp(createTmpFiles(contentTmpl, headerTmpl, file)...)
	tmpl := Load(headerTmpl.SimpleName())
	clone := tmpl.Clone()
	tmpl.Append(contentTmpl.SimpleName())
	if clone.Lookup(contentTmpl.SimpleName()) != nil {
		t.Error("Template returned by clone is affected by changes " +
			"to the template it was cloned from!")
	}
	clone.Append(file.SimpleName())
	if tmpl.Lookup(file.SimpleName()) != nil {
		t.Error("The base template was affected by it clone!")
	}
}

func TestLoadedTemplateImpl_Get(t *testing.T) {
	defer cleanUp(createTmpFiles(contentTmpl, headerTmpl)...)
	w := bytes.NewBufferString("")
	tmpl := Load(contentTmpl.SimpleName()).Get().Lookup(contentTmpl.name)
	err := tmpl.Execute(w, nil)
	if err != nil {
		t.Error(err)
	}
	// Check if an already executed template can be used as
	// base for another one.
	// If the template isn't cloned correctly this will panic.
	tmpl = Load(contentTmpl.SimpleName()).
		Append(headerTmpl.SimpleName()).
		Get().
		Lookup(contentTmpl.name)
	err = tmpl.Execute(w, nil)
	if err != nil {
		t.Error(err)
	}
}

// checkForNilAndError checks if the given template is nil or
// the given error is not nil and throws errors accordingly.
func checkForNilAndError(tmpl *template.Template, err error, t *testing.T) {
	if err != nil {
		t.Errorf("An error occured while getting the templates: %s", err)
	}
	if tmpl == nil {
		t.Error("Got nil as template!")
	}
}
