/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"strconv"
	"strings"

	"encoding/json"

	"regexp"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// TestSequenceInput contains general input that can be used
// to create a sequence.
type TestSequenceInput struct {
	InputTestSequenceName,
	InputTestSequenceDescription string
	InputTestSequencePreconditions []test.Precondition
	InputTestSequenceTestCase      string
	InputLabels                    []*string
}

// SequencePost handles requests that demand the creation of a new sequence.
func SequencePost(t handler.TestCaseGetter, tsa handler.TestSequenceAdder, tec id.TestExistenceChecker,
	labelStore handler.Labels, activityStore handler.Activities) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).CreateSequence {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		input, err := GetTestSequenceInput(r, true)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ts, err := createTestSequence(t, tec, c.Project, input)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = tsa.Add(ts)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		// ADD THE NEW ASSIGNED LABELS
		for _, labelIdString := range input.InputLabels {
			labelId, err := strconv.ParseInt(*labelIdString, 10, 64)
			if err != nil {
				errors.Handle(err, w, r)
			}

			labelStore.InsertTestLabel(labelId, ts.Id, c.Project.Id, false)
		}

		// Because we only use context/activity entities in activity-items, we need to set the new ts into the context
		c.Sequence = ts
		activityStore.LogActivity(activity.CREATE_SEQUENCE, c.GetActivityEntities(), r)

		httputil.SetHeaderValue(w, httputil.NewName, ts.Name)
		http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
	}
}

// GetTestSequenceInput gets the test sequence input from the request
func GetTestSequenceInput(r *http.Request, labelOption bool) (*TestSequenceInput, error) {

	preconditions, err := getTestSequencePreconditions(r)

	var labels []*string
	if labelOption {
		err = json.Unmarshal([]byte(r.FormValue(httputil.TestSequenceLabels)), &labels)
		if err != nil {
			return &TestSequenceInput{}, err
		}
	}

	testSequenceInput := TestSequenceInput{
		InputTestSequenceName:          r.FormValue(httputil.TestSequenceName),
		InputTestSequenceDescription:   r.FormValue(httputil.TestSequenceDescription),
		InputTestSequencePreconditions: preconditions,
		InputTestSequenceTestCase:      r.FormValue(httputil.TestSequenceTestCase),
		InputLabels:                    labels,
	}

	return &testSequenceInput, err
}

const (
	labelInvalidJSONTitle = "Unable to read labels from request."
	labelInvalidJSON      = "The labels of the testsequence couldn't be read from your request. " +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
	preconditionInvalidJSONTitle = "Unable to read preconditions from request"
	preconditionInvalidJSON      = "The preconditions of the test sequence couldn't be read from your request" +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

func getTestSequencePreconditions(r *http.Request) (preconditions []test.Precondition, err error) {
	preconditionString := r.FormValue(httputil.TestSequencePreconditions)
	if preconditionString == "" {
		return []test.Precondition{}, nil
	}
	var preconditionValues []string
	if errJSON := json.Unmarshal([]byte(preconditionString), &preconditionValues); errJSON != nil {
		preconditions = nil
		err = errors.ConstructStd(http.StatusBadRequest,
			preconditionInvalidJSONTitle, preconditionInvalidJSON, r).
			WithLogf("Client send invalid json for preconditions '%s'.", preconditionString).
			WithCause(errJSON).
			WithStackTrace(1).
			WithRequestDump(r).
			Finish()
		return preconditions, err
	}

	for _, prec := range preconditionValues {
		preconditions = append(preconditions, test.Precondition{
			Content: prec,
		})
	}
	return preconditions, nil
}

// createTestSequence tries to create a sequence from user input. If it
// it fails an error is returned instead.
func createTestSequence(t handler.TestCaseGetter, sequenceChecker id.TestExistenceChecker, p *project.Project,
	input *TestSequenceInput) (*test.Sequence, error) {

	caseNames := strings.Split(input.InputTestSequenceTestCase, "/")
	if input.InputTestSequenceTestCase == "" {
		caseNames = nil
	}

	caseIDs := make([]id.TestID, len(caseNames))
	for i, tcName := range caseNames {
		caseIDs[i] = id.NewTestID(p.ID(), tcName, true)
	}

	tcs, err := handler.GetTestCases(t, caseIDs...)
	if err != nil {
		return nil, err
	}

	//Trim all leading and trailing whitespaces and replace multiple whitespaces with a single space character
	input.InputTestSequenceName = strings.TrimSpace(input.InputTestSequenceName)
	re := regexp.MustCompile(`\s\s+`)
	input.InputTestSequenceName = re.ReplaceAllString(input.InputTestSequenceName, " ")

	ts, err := test.NewTestSequence(
		input.InputTestSequenceName,
		input.InputTestSequenceDescription,
		input.InputTestSequencePreconditions,
		tcs,
		p.ID(),
	)
	if err != nil {
		return nil, err
	}

	if err := ts.ID().Validate(sequenceChecker); err != nil {
		return nil, err
	}

	return &ts, nil
}
