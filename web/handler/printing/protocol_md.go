/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package printing

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/util"
)

// ProtocolCaseMd builds and serves a markdown string for the protocol
func ProtocolCaseMd(l handler.CaseProtocolLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		protocols, err := l.GetCaseExecutionProtocols(c.Case.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		r.ParseForm()

		protocolNr := int64(0)
		if r.FormValue(httputil.ProtocolNr) != "" {
			protocolNr, err = strconv.ParseInt(r.FormValue(httputil.ProtocolNr), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
		}

		testVersion := int64(0)
		if r.FormValue(httputil.TestVersion) != "" {
			testVersion, err = strconv.ParseInt(r.FormValue(httputil.TestVersion), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}

		}

		var protocol test.CaseExecutionProtocol
		for _, prot := range protocols {
			if prot.ProtocolNr == int(protocolNr) && prot.TestVersion.TestVersion() == int(testVersion) {
				protocol = prot
				break
			}
		}

		// check whether protocol is empty or not
		if reflect.DeepEqual(protocol, test.CaseExecutionProtocol{}) {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, "Protocol not found", "We couldn't find the requested protocol", r).
					WithLog("Invalid parameters sent by client.").
					WithStackTrace(2).
					Finish(), w, r)
			return
		}

		md, err := buildMarkdownForCaseProtocol(protocol, *c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		response, err := json.Marshal(md)
		if err != nil {
			errors.Handle(err, w, r)
		}
		_, err = w.Write(response)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

// ProtocolSequenceMd builds and serves a markdown string for the protocol
func ProtocolSequenceMd(l handler.SequenceProtocolLister, lc handler.CaseProtocolLister, tg handler.TestCaseGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		protocols, err := l.GetSequenceExecutionProtocols(c.Sequence.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		r.ParseForm()

		protocolNr := int64(0)
		if r.FormValue(httputil.ProtocolNr) != "" {
			protocolNr, err = strconv.ParseInt(r.FormValue(httputil.ProtocolNr), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
		}

		testVersion := int64(0)
		if r.FormValue(httputil.TestVersion) != "" {
			testVersion, err = strconv.ParseInt(r.FormValue(httputil.TestVersion), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
		}

		var protocol test.SequenceExecutionProtocol
		for _, prot := range protocols {
			if prot.ProtocolNr == int(protocolNr) && prot.TestVersion.TestVersion() == int(testVersion) {
				protocol = prot
				break
			}
		}

		// check whether protocol is empty or not
		if reflect.DeepEqual(protocol, test.SequenceExecutionProtocol{}) {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, "Protocol not found", "We couldn't find the requested protocol", r).
					WithLog("Invalid parameters sent by client.").
					WithStackTrace(2).
					Finish(), w, r)
			return
		}

		md, err := buildMarkdownForSequenceProtocol(protocol, *c.Sequence, lc, tg)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		response, err := json.Marshal(md)
		if err != nil {
			errors.Handle(err, w, r)
		}
		_, err = w.Write(response)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

func buildMarkdownForCaseProtocol(protocol test.CaseExecutionProtocol, tc test.Case) (string, error) {
	var markdown string
	protocolInfo := getMarkdownProtocolInfoCase(protocol, tc)
	caseInfo := getMarkdownCaseInfo(protocol, tc)
	results := getMarkdownProtocolResults(protocol, tc)

	markdown = protocolInfo + caseInfo + results
	return markdown, nil
}

func buildMarkdownForSequenceProtocol(protocol test.SequenceExecutionProtocol, ts test.Sequence, protocolLister handler.CaseProtocolLister, caseGetter handler.TestCaseGetter) (string, error) {
	var markdown string

	markdown = markdown + getMarkdownProtocolInfoSeq(protocol, ts)
	markdown = markdown + getMarkdownSequenceInfo(protocol, ts)
	markdown = markdown + "  \n  \n  \n"
	results, err := getSequenceProtocolResultsMarkdown(protocol, ts, protocolLister, caseGetter)
	markdown = markdown + results

	return markdown, err
}

func getMarkdownProtocolInfoCase(protocol test.CaseExecutionProtocol, tc test.Case) string {
	md := "## " + protocol.TestVersion.TestID.Test() + "  \n"
	md = md + "**Version:** " + strconv.Itoa(protocol.TestVersion.TestVersion()) + "  \n"
	md = md + "**Project:** " + protocol.TestVersion.Project() + "  \n\n"
	md = md + "**Result:** " + protocol.Result.PrettyString() + "  \n"
	md = md + "**Comment:** " + util.CutMarkdown(protocol.Comment) + "  \n\n"
	md = md + "**ProtocolNr:** " + strconv.Itoa(protocol.ProtocolNr) + "  \n"
	md = md + "**Executed on:** " + protocol.ExecutionDate.String() + "  \n"
	if protocol.IsAnonymous {
		md = md + "**Tester:** " + "Anonymous" + "  \n"
	} else {
		md = md + "**Tester:** " + protocol.UserName + "  \n"
	}
	md = md + "**SUTVersion:** " + protocol.SUTVersion + "  \n"
	md = md + "**SUTVariant:** " + protocol.SUTVariant + "  \n\n"
	return md
}
func getMarkdownCaseInfo(protocol test.CaseExecutionProtocol, tc test.Case) string {
	md := "**Description:** " + tc.TestCaseVersions[len(tc.TestCaseVersions)-protocol.TestVersion.TestVersion()].Description + "  \n"

	md = md + "**Preconditions:**  \n\n"
	md = md + getPreconditionsMarkdown(protocol.PreconditionResults)

	return md
}
func getMarkdownProtocolResults(protocol test.CaseExecutionProtocol, tc test.Case) string {
	md := "  \n  \n### Test Step Results:  \n"

	for index, testStepProtocol := range protocol.StepProtocols {
		md = md + "#### " + strconv.Itoa(index+1) + "." + tc.TestCaseVersions[len(tc.TestCaseVersions)-protocol.TestVersion.TestVersion()].Steps[index].Action + "  \n"
		md = md + "+ " + "**Result:** " + util.CutMarkdown(testStepProtocol.Result.PrettyString()) + "  \n"
		md = md + "+ " + "**Expected Result:** " + util.CutMarkdown(tc.TestCaseVersions[len(tc.TestCaseVersions)-protocol.TestVersion.TestVersion()].Steps[index].ExpectedResult) + "  \n"
		md = md + "+ " + "**Actual Result:** " + util.CutMarkdown(testStepProtocol.ObservedBehavior) + "  \n\n"
		// TODO: Figure out whether or not to add attachments (how to add local attachments?)
	}
	return md
}

func getMarkdownProtocolInfoSeq(protocol test.SequenceExecutionProtocol, ts test.Sequence) string {
	markdown := "# " + protocol.TestVersion.TestID.Test() + "  \n"
	markdown = markdown + "**Version:** " + strconv.Itoa(protocol.TestVersion.TestVersion()) + "  \n"
	markdown = markdown + "**Project:** " + protocol.TestVersion.Project() + "  \n  \n"
	markdown = markdown + "**Result:** " + protocol.Result.PrettyString() + "  \n  \n"
	markdown = markdown + "**ProtocolNr:** " + strconv.Itoa(protocol.ProtocolNr) + "  \n"
	markdown = markdown + "**Executed on:** " + protocol.ExecutionDate.String() + "  \n"
	if protocol.IsAnonymous {
		markdown = markdown + "**Tester:** " + "Anonymous" + "  \n"
	} else {
		markdown = markdown + "**Tester:** " + protocol.UserName + "  \n"
	}
	markdown = markdown + "**SUTVersion:** " + protocol.SUTVersion + "  \n"
	markdown = markdown + "**SUTVariant:** " + protocol.SUTVariant + "  \n  \n"

	return markdown
}

func getMarkdownSequenceInfo(protocol test.SequenceExecutionProtocol, ts test.Sequence) string {
	markdown := "**Description:** " + ts.SequenceVersions[len(ts.SequenceVersions)-protocol.TestVersion.TestVersion()].Description + "  \n"
	markdown = markdown + "**Preconditions:**  \n\n"
	markdown = markdown + getPreconditionsMarkdown(protocol.PreconditionResults)

	return markdown
}

func getPreconditionsMarkdown(results []test.PreconditionResult) string {
	var md string
	for _, preconditionResult := range results {
		md = md + "- " + preconditionResult.Precondition.Content + " *(" + preconditionResult.Result + ")*  \n"
	}
	return md
}

func getSequenceProtocolResultsMarkdown(protocol test.SequenceExecutionProtocol, ts test.Sequence, protocolLister handler.CaseProtocolLister, caseGetter handler.TestCaseGetter) (string, error) {
	var markdown string

	for _, testCaseProtocolID := range protocol.CaseExecutionProtocols {
		testCaseProtocol, err := protocolLister.GetCaseExecutionProtocol(testCaseProtocolID)
		if err != nil {
			return markdown, err
		}
		testCase, ex, err := caseGetter.Get(testCaseProtocol.TestVersion.TestID)
		if err != nil {
			return markdown, err
		}
		if !ex {
			return markdown, fmt.Errorf("Test Case not found: %v", testCaseProtocol.TestVersion.TestID)
		}

		markdown = markdown + getMarkdownProtocolInfoCase(testCaseProtocol, *testCase)
		markdown = markdown + getMarkdownCaseInfo(testCaseProtocol, *testCase)
		markdown = markdown + getMarkdownProtocolResults(testCaseProtocol, *testCase)
		markdown = markdown + "  \n  \n"
	}
	return markdown, nil
}
