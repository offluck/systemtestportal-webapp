/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/integration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// TestCaseAdder provides everything needed to add a testcase to a storage.
type TestCaseAdder interface {
	// Add stores a test case for the given project under the given container
	Add(testCase *test.Case) error
}

// TestCaseDeleter provides everything needed to remove a testcase to a storage.
type TestCaseDeleter interface {
	// Delete removes the test case with the given ID for the given project under the given container
	Delete(testCase *test.Case) error
}

// TestCaseRenamer provides a function for renaming a test case
type TestCaseRenamer interface {
	// Rename changes the name of a given test case to the new name
	Rename(old, new id.TestID) error
}

// TestCaseUpdater contains every method needed to update a testcase.
type TestCaseUpdater interface {
	TestCaseDeleter
	TestCaseAdder
	TestCaseRenamer
}

// ProjectAdder is used to add projects to the storage.
type ProjectAdder interface {
	// Add stores a new project. The first argument is used to identify the container for the project
	Add(*project.Project) error
}

// GroupAdder is used to add groups to the storage.
type GroupAdder interface {
	// Add stores a group in the system
	Add(group *group.Group) error
}

// TestSequenceAdder is used to add testsequences to the storage.
type TestSequenceAdder interface {
	// Add stores a test sequence for the given project under the given container
	Add(testSequence *test.Sequence) error
}

// TestCaseGetter is used to get testcases from the store.
type TestCaseGetter interface {
	// Get returns the test case with the given ID for the given project under the given container
	Get(caseID id.TestID) (*test.Case, bool, error)
}

// TestCaseLister is used to list all testcases for a specific project.
type TestCaseLister interface {
	// List returns a list of test cases for the given project under the given container
	List(projectID id.ProjectID) ([]*test.Case, error)
}

//ProjectRoleLister is used to list all roles for a specific project.
type ProjectRoleLister interface {
	// List returns a list of roles for the given project under the given container
	List(projectID id.ProjectID) ([]*project.Role, error)
}

// GroupLister is used to list all groups in the system.
type GroupLister interface {
	// List returns a list of groups in the system
	List() ([]*group.Group, error)
}

// ProjectLister is used to list all projects in the system.
type ProjectLister interface {
	// ListForOwner returns the projects for an owner
	ListForOwner(id.ActorID) ([]*project.Project, error)
	// ListForMember returns all projects where the actor is a member
	ListForMember(id.ActorID) ([]*project.Project, error)
	// ListForActor returns all projects that the actor has access to
	ListForActor(id.ActorID) ([]*project.Project, error)
	// ListAll returns all projects in the system
	ListAll() ([]*project.Project, error)
	// ListPublic returns all public projects in the system
	ListPublic() ([]*project.Project, error)
	// ListInternal returns all internal projects in the system
	ListInternal() ([]*project.Project, error)
	// ListPrivate returns all private projects for an actor
	ListPrivate(id.ActorID) ([]*project.Project, error)
}

// UserLister is used to list all users in the system.
type UserLister interface {
	// List returns a list of users in the system
	List() ([]*user.User, error)
}

//UserAdder is used to add users
type UserAdder interface {
	Add(pur *user.PasswordUser) error
}

//UserUpdater is used to update users
type UserUpdater interface {
	Update(pur *user.User) error
	Deactivate(user *user.User, isDelete bool) error
}

//UserValidator is used to Validate an user
type UserValidator interface {
	Validate(identifier, password string) (*user.User, bool, error)
}

// TaskListAdder is used to add a to do list to the storage
type TaskListAdder interface {
	// Add saves a to do-list to the system
	Add(list task.List) error
}

// TaskListGetter is used to retrieve to do-lists from the store
type TaskListGetter interface {
	// Get returns the to do-list of a user. If the user does not have one,
	// an empty list is returned
	Get(username string) (*task.List, error)
}

// TaskGetter is used to retrieve tasks
type TaskGetter interface {
	// GetTasksForTest retrieves the open tasks for a test, identified by the testID
	GetTasksForTest(testID id.TestID) ([]*task.Item, error)
}

// TaskAdder is used to add tasks to the store
type TaskAdder interface {
	// AddItem saves an item to the store
	AddItem(item task.Item) error
}

// TestSequenceLister is used to list all testsequences for a specific project.
type TestSequenceLister interface {
	// List returns a list of test sequences for the given project under the given container
	List(projectID id.ProjectID) ([]*test.Sequence, error)
}

// TestSequenceGetter returns the test.Sequence for the testId
type TestSequenceGetter interface {
	Get(testId id.TestID) (*test.Sequence, bool, error)
}

// ProtocolCaseRenamer is used to handle renames.
type ProtocolCaseRenamer interface {
	// HandleCaseRename updates the protocol store after a test case has been renamed
	HandleCaseRename(old, new id.TestID) error
}

// ProjectDeleter is used to delete the reference of an existing project from storage
type ProjectDeleter interface {
	// Delete removes an existing project. The first argument is used to identify the container
	// and the second to identify the project
	Delete(projectID id.ProjectID) error
}

// TestSequenceUpdater is used to update testsequences in the storage.
type TestSequenceUpdater interface {
	TestSequenceAdder
	TestSequenceRenamer
	TestSequenceDeleter
}

// TestSequenceRenamer provides a function for renaming a test case
type TestSequenceRenamer interface {
	// Rename changes the name of a given test sequence to the new name
	Rename(old, new id.TestID) error
}

// SequenceRenameHandler defines an interface clients need to fulfill to handle renames of test sequences
type SequenceRenameHandler interface {
	// HandlerSequenceRename allows dependents to handle renames of test sequences
	HandleSequenceRename(old, new id.TestID) error
}

// TestSequenceDeleter is used to delete testsequences from the storage.
type TestSequenceDeleter interface {
	// Delete removes the testsequence with the given ID for the given project under the given container
	Delete(testSequence *test.Sequence) error
}

// CaseProtocolLister is used to list testcase execution protocols.
type CaseProtocolLister interface {
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error)
	GetCaseExecutionProtocolsForProject(projectID id.ProjectID) ([]test.CaseExecutionProtocol, error)
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
}

// SequenceProtocolLister is used to list testsequence execution protocols.
type SequenceProtocolLister interface {
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(sequenceID id.TestID) ([]test.SequenceExecutionProtocol, error)
	GetSequenceExecutionProtocolsForProject(projectID id.ProjectID) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error)
}

type ProjectRoleUpdater interface {
	ProjectRoleAdder
	ProjectRoleDeleter
}

type ProjectRoleAdder interface {
	Add(role *project.Role) error
}

type ProjectRoleDeleter interface {
	Delete(role *project.Role) error
}

// CommentAdder is required to use the comment store from the display-handler package. Do not delete this.
type CommentAdder interface {
	InsertComment(comment *comment.Comment, testObject interface{}, user *user.User) error
}

type SystemSettings interface {
	GetSystemSettings() (*system.SystemSettings, error)
	UpdateSystemSettings(systemSettings *system.SystemSettings) error
	InsertSystemSettings(systemSettings *system.SystemSettings) error
}

type Comments interface {
	GetComment(commentId int64, project *project.Project) (*comment.Comment, error)
	UpdateComment(*comment.Comment) error
	CommentAdder
	GetCommentsForTest(testObject interface{}, project *project.Project, requester *user.User) ([]*comment.Comment, error)
	DeleteCommentsForTest(testObject interface{}) error
}
type ProjectLabelUpdater interface {
	ProjectAdder
	ProjectLabelRenamer
}

type ProjectLabelRenamer interface {
	RenameProjectLabel(*project.Project, project.Label, project.Label) error
}

type Labels interface {
	GetLabelsForProject(testProject *project.Project) ([]*project.Label, error)
	DeleteLabel(labelId int64) error
	UpdateLabelForProject(label *project.Label, projectId int64) (int64, error)

	GetLabelsForTest(testObject interface{}, projectId int64) ([]*project.Label, error)
	DeleteTestLabel(labelId int64, testId int64, projectId int64, isCase bool) error
	InsertTestLabel(labelId int64, testId int64, projectId int64, isCase bool) error
	GetAllTestLabels() ([]*project.TestLabel, error)
}

type Activities interface {
	LogActivity(activityType int, activityEntities *activity.ActivityEntities, request *http.Request) error
	GetActivitiesForProjectLimitFromOffset(projectId int64, limit int, offset int) ([]*activity.Activity, error)
}
type IssueAdder interface {
	AddIssue(issue integration.GitLabIssue) error
}
type ProjectSUTVersionUpdater interface {
	ProjectAdder
	ProjectVersionRenamer
	ProjectVariantRenamer
}

type IssueGetter interface {
	GetIssue(testCaseID id.TestID, version string, variant string) (*integration.GitLabIssue, error)
}
type ProjectVersionRenamer interface {
	RenameProjectVersion(project *project.Project, oldVersionName, newVersionName string) error
}

type ProjectVariantRenamer interface {
	RenameProjectVariant(project *project.Project, versionName, oldVariantName, newVariantName string) error
}
