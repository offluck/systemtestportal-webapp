// +build ignore

/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"testing"

	"net/http"

	"net/url"

	"net/http/httptest"

	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	project2 "gitlab.com/stp-team/systemtestportal-webapp/store/project"
	test2 "gitlab.com/stp-team/systemtestportal-webapp/store/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

const emptyBodyErrorMessage = "Answer body is empty, but shouldn't."
const getProtocolErrorMessage = "Couldn't get current protocol from session store."
const noProtocolInSessionErrorMessage = "No protocol was saved to session store."
const ProtocolInSessionErrorMessage = "Expected no Protocol in session store, but found %v+."
const sutVariantErrorMessage = "SUTVariant wasn't transferred correctly to new Protocol. " +
	"Value in Protocol is %q but expected %q"
const sutVersionErrorMessage = "SUT-Version wasn't transferred correctly to new Protocol. " +
	"Value in Protocol is %q but expected %q"
const resultsErrorMessage = "Result wasn't saved correctly to Protocol. Result in Protocol is %q but expected %q"
const commentErrorMessage = "Comment wasn't saved correctly to Protocol. Comment in Protocol is %q but expected %q"
const observedBehaviourErrorMessage = "Observed behaviour wasn't saved correctly to Protocol. " +
	"Observed behaviour in Protocol is %q but expected %q"
const neededTimeErrorMessage = "NeededTime wasn't saved correctly to Protocol. " +
	"NeededTime in Protocol is %v+ but expected %v+"
const visitedErrorMessage = "Saved test step was marked as not visited, but was visited"

const sutVariant = "Win 10"
const sutVersion = "2.0.1"

var startTime duration.Duration
var p *project.Project
var tc *test.Case
var tcv test.CaseVersion
var tcr test.CaseExecutionProtocol
var ts *test.Sequence
var tsv test.SequenceVersion
var tsr test.SequenceExecutionProtocol
var caseNrLast int
var tcLast test.Case
var tcvLast test.CaseVersion
var tcrLast test.CaseExecutionProtocol
var tcPath string
var tsPath string

func setup(sessionTcr *test.CaseExecutionProtocol, sessionTsr *test.SequenceExecutionProtocol) {

	//Renew global variable
	startTime = duration.NewDuration(1, 1, 1)
	p, _, _ = project2.GetStore().Get(id.NewProjectID(id.NewActorID("user"), "DuckDuckGo.com"))
	tc, _, _ = test2.GetCaseStore().Get(id.NewTestID(p.ID(), "Change Theme"))
	tcv = tc.TestCaseVersions[0]
	tcr, _ = test.NewCaseExecutionProtocol(*tc, len(tc.TestCaseVersions), p.ID, "Windows 10",
		"1.0.1", startTime)
	ts, _, _ = test2.GetSequenceStore().Get(id.NewTestID(p.ID(), "Settings"))
	tsv = ts.SequenceVersions[0]
	tsr, _ = test.NewSequenceExecutionProtocol(*ts, len(ts.SequenceVersions), p.ID, sutVersion, sutVariant, startTime)
	caseNrLast = len(tsv.Cases)
	tcLast = tsv.Cases[caseNrLast-1]
	tcvLast = tcLast.TestCaseVersions[0]
	tcrLast, _ = test.NewCaseExecutionProtocol(tcLast, len(tcLast.TestCaseVersions), p.ID, sutVariant,
		sutVersion, startTime)
	tcPath = "/user/" + p.ID + "/testcases/" + tc.ID + "/execute"
	tsPath = "/user/" + p.ID + "/testsequences/" + ts.ID + "/execute"

	//Setup Session Store
	sessions.InitSessionManagement(&sessions.TestStore{}, nil)
	time := duration.NewDuration(0, 0, 0)

	if sessionTcr != nil {
		sessions.GetSessionStore().SetCurrentCaseProtocol(nil, nil, sessionTcr)
		time = time.Add(tcr.GetNeededTime())
	}
	if sessionTsr != nil {
		sessions.GetSessionStore().SetCurrentSequenceProtocol(nil, nil, sessionTsr)
		time = time.Add(tsr.OtherNeededTime)
	}
	sessions.GetSessionStore().SetDuration(nil, nil, &time)
	context.Init(sessions.GetSessionStore())
}

func TestGetCase(t *testing.T) {
	setup(nil, nil)
	getRequest(t, tcPath, http.StatusOK, true, ProjectHandler)
}
func TestPostCaseStart(t *testing.T) {
	setup(nil, nil)

	const hour = 1
	const min = 4
	const sec = 30
	timeDelta := duration.NewDuration(hour, min, sec)
	timeNew := timeDelta

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "0")
	values.Add(keyCaseNr, "0")
	values.Add(keySUTVariant, sutVariant)
	values.Add(keySUTVersion, sutVersion)
	values.Add(keyHours, strconv.Itoa(int(timeNew.Hours())))
	values.Add(keyMinutes, strconv.Itoa(timeNew.GetMinuteInHour()))
	values.Add(keySeconds, strconv.Itoa(timeNew.GetSecondInMinute()))

	req, _ := testPostRequest(t, tcPath, values, ProjectHandler, http.StatusOK, false)
	prt := getCurrentCaseProtocol(t, req, false)
	if prt.SUTVariant != sutVariant {
		t.Errorf(sutVariantErrorMessage, prt.SUTVariant, sutVariant)
	}
	if prt.SUTVersion != sutVersion {
		t.Errorf(sutVersionErrorMessage, prt.SUTVersion, sutVersion)
	}
	if prt.OtherNeededTime != timeDelta {
		t.Errorf(neededTimeErrorMessage, prt.OtherNeededTime, timeDelta)
	}
}

func testPostStep(t *testing.T, stepNr int, caseNr int, path string) {
	const result = test.PartiallySuccessful
	const comment = "What's that? I think should write a comment."
	const observed = "Was all ok, but very slow."
	const hour = 1
	const min = 4
	const sec = 30
	timeDelta := duration.NewDuration(hour, min, sec)
	timeNew := timeSoFar().Add(timeDelta)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, strconv.Itoa(caseNr))
	values.Add(keyResult, strconv.Itoa(result.Integer()))
	values.Add(keyComment, comment)
	values.Add(keyObservedBehavior, observed)
	values.Add(keyHours, strconv.Itoa(int(timeNew.Hours())))
	values.Add(keyMinutes, strconv.Itoa(timeNew.GetMinuteInHour()))
	values.Add(keySeconds, strconv.Itoa(timeNew.GetSecondInMinute()))

	req, _ := testPostRequest(t, path, values, ProjectHandler, http.StatusOK, false)

	prt := getCurrentCaseProtocol(t, req, false)
	stepPrt := prt.StepProtocols[stepNr-1]

	if !stepPrt.Visited {
		t.Error(visitedErrorMessage)
	}
	if stepPrt.Result != result {
		t.Errorf(resultsErrorMessage, stepPrt.Result, result)
	}
	if stepPrt.Comment != comment {
		t.Errorf(commentErrorMessage, stepPrt.Comment, comment)
	}
	if stepPrt.ObservedBehavior != observed {
		t.Errorf(observedBehaviourErrorMessage, stepPrt.ObservedBehavior, observed)
	}
	if stepPrt.NeededTime != timeDelta {
		t.Errorf(neededTimeErrorMessage, stepPrt.NeededTime, timeDelta)
	}
}

func TestPostCaseStep(t *testing.T) {
	setup(&tcr, nil)

	testPostStep(t, 1, 0, tcPath)
	testPostStep(t, len(tcv.Steps), 0, tcPath)
}

func TestPostCaseSummary(t *testing.T) {
	setup(&tcr, nil)

	stepNr := len(tcv.Steps) + 1
	const comment = "I'm not motivated anymore for further testing!"
	const result = test.Pass
	const hour = 1
	const min = 4
	const sec = 30
	timeDelta := duration.NewDuration(hour, min, sec)
	timeNew := timeSoFar().Add(timeDelta)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, "0")
	values.Add(keyComment, comment)
	values.Add(keyResult, result.String())
	values.Add(keyHours, strconv.Itoa(int(timeNew.Hours())))
	values.Add(keyMinutes, strconv.Itoa(timeNew.GetMinuteInHour()))
	values.Add(keySeconds, strconv.Itoa(timeNew.GetSecondInMinute()))

	req, _ := testPostRequest(t, tcPath, values, ProjectHandler, http.StatusSeeOther, true)
	getCurrentCaseProtocol(t, req, true)
	prtSlice, err := store.GetProtocolStore().GetCaseExecutionProtocols(p.ID, tc.ID)
	if err != nil {
		t.Error(err)
	}
	prt := prtSlice[len(prtSlice)-1]
	if prt.Comment != comment {
		t.Errorf(commentErrorMessage, prt.Comment, comment)
	}
	if prt.Result != result {
		t.Errorf(resultsErrorMessage, prt.Result, result)
	}
	if prt.OtherNeededTime != timeDelta.Add(startTime) {
		t.Errorf(neededTimeErrorMessage, prt.OtherNeededTime, timeDelta.Add(startTime))
	}
}

func TestGetSequence(t *testing.T) {
	setup(nil, nil)
	getRequest(t, tsPath, http.StatusOK, true, ProjectHandler)
}
func TestPostSequenceStart(t *testing.T) {
	setup(nil, nil)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "0")
	values.Add(keyCaseNr, "0")
	values.Add(keySUTVariant, sutVariant)
	values.Add(keySUTVersion, sutVersion)

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	prt := getCurrentSequenceProtocol(t, req, false)
	if prt.SUTVariant != sutVariant {
		t.Errorf(sutVariantErrorMessage, prt.SUTVariant, sutVariant)
	}
	if prt.SUTVersion != sutVersion {
		t.Errorf(sutVersionErrorMessage, prt.SUTVersion, sutVersion)
	}
}
func TestPostSequenceCaseStart(t *testing.T) {
	setup(nil, &tsr)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "0")
	values.Add(keyCaseNr, "1")
	values.Add(keySUTVariant, sutVariant)
	values.Add(keySUTVersion, sutVersion)

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	prt := getCurrentCaseProtocol(t, req, false)
	if prt.SUTVariant != sutVariant {
		t.Errorf(sutVariantErrorMessage, prt.SUTVariant, sutVariant)
	}
	if prt.SUTVersion != sutVersion {
		t.Errorf(sutVersionErrorMessage, prt.SUTVersion, sutVersion)
	}
}

func TestPostSequenceStep(t *testing.T) {
	setup(&tcr, &tsr)
	testPostStep(t, 1, 1, tsPath)
	testPostStep(t, 2, 1, tsPath)
	setup(&tcrLast, &tsr)
	testPostStep(t, len(tcv.Steps), 1, tsPath)
	testPostStep(t, len(tcv.Steps), len(tsv.Cases), tsPath)
}

func getCurrentCaseProtocol(t *testing.T, r http.Request, expectedEmpty bool) *test.CaseExecutionProtocol {
	prt, err := sessions.GetSessionStore().GetCurrentCaseProtocol(&r)
	if err != nil {
		t.Error(getProtocolErrorMessage)
	}
	if !expectedEmpty && prt == nil {
		t.Error(noProtocolInSessionErrorMessage)
	}
	if expectedEmpty && prt != nil {
		t.Errorf(ProtocolInSessionErrorMessage, prt)
	}
	return prt
}
func TestPostSequenceCaseSummary(t *testing.T) {
	setup(&tcr, &tsr)

	const comment = "I'm not motivated anymore for further testing!"
	const result = test.Pass
	const hour = 1
	const min = 4
	const sec = 30
	timeDelta := duration.NewDuration(hour, min, sec)
	timeNew := timeSoFar().Add(timeDelta)

	var stepNr = len(tcv.Steps) + 1

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, "1")
	values.Add(keyComment, comment)
	values.Add(keyResult, result.String())
	values.Add(keyHours, strconv.Itoa(int(timeNew.Hours())))
	values.Add(keyMinutes, strconv.Itoa(timeNew.GetMinuteInHour()))
	values.Add(keySeconds, strconv.Itoa(timeNew.GetSecondInMinute()))

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	getCurrentCaseProtocol(t, req, true)
	getCurrentSequenceProtocol(t, req, false)
	recSlice, err := store.GetProtocolStore().GetCaseExecutionProtocols(p.ID, tc.ID)
	if err != nil {
		t.Error(err)
	}
	rec := recSlice[len(recSlice)-1]
	if rec.Comment != comment {
		t.Errorf(commentErrorMessage, rec.Comment, comment)
	}
	if rec.Result != result {
		t.Errorf(resultsErrorMessage, rec.Result, result)
	}
	if rec.OtherNeededTime != timeDelta.Add(startTime) {
		t.Errorf(neededTimeErrorMessage, rec.OtherNeededTime, timeDelta.Add(startTime))
	}
}
func TestPostSequenceLastCaseSummary(t *testing.T) {
	tsProtocol, _ := store.GetProtocolStore().GetSequenceExecutionProtocol(p.ID, ts.ID, 1)
	tsProtocol.SUTVersion = sutVersion
	tsProtocol.SUTVariant = sutVariant
	tsProtocol.ID = 0
	setup(&tcrLast, &tsProtocol)

	const comment = "I'm not motivated anymore for further testing!"
	const result = test.Pass
	const hour = 1
	const min = 4
	const sec = 30
	timeDelta := duration.NewDuration(hour, min, sec)
	timeNew := timeSoFar().Add(timeDelta)

	var stepNr = len(tcvLast.Steps) + 1

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, strconv.Itoa(stepNr))
	values.Add(keyCaseNr, strconv.Itoa(caseNrLast))
	values.Add(keyComment, comment)
	values.Add(keyResult, result.String())
	values.Add(keyHours, strconv.Itoa(int(timeNew.Hours())))
	values.Add(keyMinutes, strconv.Itoa(timeNew.GetMinuteInHour()))
	values.Add(keySeconds, strconv.Itoa(timeNew.GetSecondInMinute()))

	req, _ := testPostRequest(t, tsPath, values, ProjectHandler, http.StatusOK, false)
	getCurrentCaseProtocol(t, req, true)
	getCurrentSequenceProtocol(t, req, true)

	//check case protocol
	casePrtSlice, err := store.GetProtocolStore().GetCaseExecutionProtocols(p.ID, tcLast.ID)
	if err != nil {
		t.Error(err)
	}
	CasePrt := casePrtSlice[len(casePrtSlice)-1]
	if CasePrt.Comment != comment {
		t.Errorf(commentErrorMessage, CasePrt.Comment, comment)
	}
	if CasePrt.Result != result {
		t.Errorf(resultsErrorMessage, CasePrt.Result, result)
	}
	if CasePrt.OtherNeededTime != timeDelta.Add(startTime) {
		t.Errorf(neededTimeErrorMessage, CasePrt.OtherNeededTime, timeDelta.Add(startTime))
	}

	//check sequence protocol
	seqPrtSlice, err := store.GetProtocolStore().GetSequenceExecutionProtocols(p.ID, ts.ID)
	if err != nil {
		t.Error(err)
	}
	SeqPrt := seqPrtSlice[len(seqPrtSlice)-1]
	if SeqPrt.SUTVariant != sutVariant {
		t.Errorf(sutVariantErrorMessage, SeqPrt.SUTVariant, sutVariant)
	}
	if SeqPrt.SUTVersion != sutVersion {
		t.Errorf(sutVersionErrorMessage, SeqPrt.SUTVersion, sutVersion)
	}
}
func TestPostSequenceSummary(t *testing.T) {
	setup(nil, nil)

	values := url.Values{}
	values.Add("fragment", "1")
	values.Add(keyStepNr, "1")
	values.Add(keyCaseNr, "0")

	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusSeeOther, true)
}

func TestBadRequest(t *testing.T) {
	setup(nil, nil)
	testPostRequestEmpty(t, tcPath, http.StatusBadRequest)
	testPostRequestEmpty(t, tsPath, http.StatusBadRequest)
	values := url.Values{}
	values.Add(keyStepNr, strconv.Itoa(len(tcv.Steps)+5))
	testPostRequest(t, tcPath, values, ProjectHandler, http.StatusBadRequest, true)
	values = url.Values{}
	values.Add(keyCaseNr, "0")
	values.Add(keyStepNr, "2")
	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusBadRequest, true)
	values = url.Values{}
	values.Add(keyCaseNr, strconv.Itoa(len(tsv.Cases)+1))
	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusBadRequest, true)
	values = url.Values{}
	values.Add(keyCaseNr, "1")
	values.Add(keyStepNr, "200")
	testPostRequest(t, tsPath, values, ProjectHandler, http.StatusBadRequest, true)
}

func getCurrentSequenceProtocol(t *testing.T, r http.Request, expectedEmpty bool) *test.SequenceExecutionProtocol {
	rec, err := sessions.GetSessionStore().GetCurrentSequenceProtocol(&r)
	if err != nil {
		t.Error(getProtocolErrorMessage)
	}
	if !expectedEmpty && rec == nil {
		t.Error(noProtocolInSessionErrorMessage)
	}
	if expectedEmpty && rec != nil {
		t.Errorf(ProtocolInSessionErrorMessage, rec)
	}
	return rec
}

func testPostRequest(t *testing.T, path string, formValues url.Values, handler http.HandlerFunc,
	statusWant int, emptyAnswerBodyOk bool) (http.Request, httptest.ResponsePrtorder) {
	req, err := http.NewRequest(http.MethodPost, path, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Form = formValues
	rec := httptest.NewRecorder()
	handler.ServeHTTP(rec, req)

	if statusWant != rec.Code {
		t.Errorf("Returned wrong status code. Expected %v but got %v", statusWant, rec.Code)
	}
	if !emptyAnswerBodyOk && rec.Body.Len() == 0 {
		t.Error(emptyBodyErrorMessage)
	}
	return *req, *rec
}

func timeSoFar() duration.Duration {
	result, _ := sessions.GetSessionStore().GetDuration(nil)
	return *result
}
