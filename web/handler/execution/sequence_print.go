/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"encoding/json"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// sequenceExecutionPrinter is used to print the pages for a testsequence execution.
type sequenceExecutionPrinter struct {
	CaseProtocolGetter
}

// printStartPage prints the start page for the execution of a testsequence.
func (printer *sequenceExecutionPrinter) printStartPage(w http.ResponseWriter, r *http.Request,
	tsv test.SequenceVersion) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Sequence == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	tsvp, err := test.UpdateInfo(&tsv)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionStartPage)
	min, hours := tsv.ItemEstimatedDuration()
	time := duration.Duration{}
	if getFormValueBool(r, keyIsPrevious) {
		time, err = getCurrentDuration(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}

	progressTotal := totalWorkSteps(tsv)
	progressPercent := int(1 / float64(progressTotal) * 100.0)

	// If the execution is started by a task, the version and variant are set via parameter
	var sutVersion, sutVariant = r.FormValue(keyVersion), r.FormValue(keyVariant)
	var isTaskExecution = false
	if sutVersion != "" && sutVariant != "" {
		isTaskExecution = true
	}

	protocol := prepareSequenceProtocol(c, tsv)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, true).
		With(keyIsInSeq, true).
		With(keyCaseNr, 0).
		With(keyProject, c.Project).
		With(keyTestObject, c.Sequence).
		With(keyTestObjectVersion, tsvp).
		With(keyProgress, 1).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyEstimatedMinutes, min).
		With(keyEstimatedHours, hours).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute()).
		With(keySeqProtocol, protocol).
		With(keySUTVersion, sutVersion).
		With(keySUTVariant, sutVariant).
		With(keyIsTaskExecution, isTaskExecution)

	handler.PrintTmpl(ctx, tmpl, w, r)
}

// printSummaryPage prints a summary page for the execution of a testsequence.
func (printer *sequenceExecutionPrinter) printSummaryPage(w http.ResponseWriter, r *http.Request,
	tsv test.SequenceVersion, protocolLister test.ProtocolLister,
	seqPrt *test.SequenceExecutionProtocol) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Sequence == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionSummary)

	if seqPrt == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadSummaryPage, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	time, err := getCurrentDuration(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	type caseSummary struct {
		Result     test.Result
		Name       string
		Comment    string
		NeededTime duration.Duration
	}

	var cases []caseSummary
	/*
		for i := 0; i < len(tsv.Cases); i++ {
			casePrt, err := protocolLister.GetCaseExecutionProtocol(seqPrt.CaseExecutionProtocols[i])

			if err != nil {
				errors.ConstructStd(http.StatusInternalServerError,
					failedSave, unableToLoadSummaryPage, r).
					WithLog("Error while trying to get protocol.").
					WithStackTrace(1).
					WithCause(err).
					WithRequestDump(r).
					Respond(w)
				return
			}
			cases = append(cases, caseSummary{
				Result:     casePrt.Result,
				Name:       tsv.Cases[i].Name,
				Comment:    casePrt.Comment,
				NeededTime: casePrt.GetNeededTime(),
			})
		}*/

	var caseProtocols []test.CaseExecutionProtocol
	caseProtocolsJson := r.FormValue(keyCaseProtocols)
	err = json.Unmarshal([]byte(caseProtocolsJson), &caseProtocols)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	for i := 0; i < len(tsv.Cases); i++ {
		cases = append(cases, caseSummary{
			Result:     caseProtocols[i].Result,
			Name:       caseProtocols[i].TestVersion.Test(),
			Comment:    caseProtocols[i].Comment,
			NeededTime: caseProtocols[i].GetNeededTime(),
		})
	}

	progressPercent := int(float64(totalWorkSteps(tsv)-1) / float64(totalWorkSteps(tsv)) * 100)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, true).
		With(keyIsInSeq, true).
		With(keyProject, c.Project).
		With(keyTestObjectVersion, tsv).
		With(keyTestObject, c.Sequence).
		With(keyAllSubObjects, cases).
		With(keyCaseNr, 0).
		With(keyStepNr, 1).
		With(keyProgress, totalWorkSteps(tsv)-1). // -1 because reviewing the execution is also a step
		With(keyProgressTotal, totalWorkSteps(tsv)).
		With(keyProgressPercent, progressPercent).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute()).
		With(keySeqProtocol, seqPrt)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

func prepareSequenceProtocol(c *handler.ContextEntities, tsv test.SequenceVersion) test.SequenceExecutionProtocol {
	protocol := test.SequenceExecutionProtocol{}

	protocol.PreconditionResults = make([]test.PreconditionResult, 0)

	for _, prec := range tsv.Preconditions {
		precResult := test.PreconditionResult{
			Precondition: prec,
			Result:       "",
		}
		protocol.PreconditionResults = append(protocol.PreconditionResults, precResult)
	}

	protocol.UserName = c.User.DisplayName
	protocol.TestVersion = tsv.ID()

	return protocol
}
