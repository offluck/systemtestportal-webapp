/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package usersession

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

const (
	unableToCheckSession = "We were unable to check the current session status. " +
		"The cookie monster probably stole your cookies :o"
)

// Handler is used to start sessions for a user and end them.
type Handler interface {
	// StartFor starts the session for given user.
	StartFor(w http.ResponseWriter, r *http.Request, u *user.User) error
	// EndFor ends the session for given user.
	EndFor(w http.ResponseWriter, r *http.Request, u *user.User) error
	// GetCurrent gets the user that hold the session. If there is no
	// user session the returned user will be nil.
	GetCurrent(r *http.Request) (*user.User, error)
}

// Auth is used to authenticate users.
type Auth interface {
	// Validate validates user credentials and returns the corresponding user.
	//
	// If the credentials are invalid false and nil(for user and error) will be
	// returned.
	//
	// If a real error happens during validation it will be returned and the
	// user will be nil and the bool false.
	Validate(identifier string, password string) (*user.User, bool, error)
}
