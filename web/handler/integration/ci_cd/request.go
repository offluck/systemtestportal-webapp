/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package ci_cd

import (
	"encoding/json"
	"net/http"
	"sort"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// CIRequestBody represents the body of the
// request. It is used for json decoding.
type CIRequestBody struct {
	Version string
	Variant string
	Token   string
}

// IntegrationPost handles the requests from the ci/cd
// of an external project that has integrated the stp.
//
// SOME BACKGROUND INFORMATION:
//
// In the ci/cd pipeline, a stage for the manual tests
// is started.
// This starts a docker container containing the application
// https://gitlab.com/schneisn/stp-ci-integration.
// This app sends requests periodically to the stp to determine
// the state of the manual tests, which is done by this function.
//
// AUTOMATIC TEST ASSIGNMENT:
//
// Tests are automatically assigned to the testers for the given version.
//
// If the version does not exist, the test-managers are given a task to
// assign this version to the correct test-cases.
// If this task is done,
// this function knows which tests can be assigned to the testers.
//
// TOKEN:
//
// The request has to contain the correct token to access the project.
// If it doesn't, the function returns a status forbidden.
//
// MEANING OF THE RESPONSE CODES:
//
// Status OK 						(200): All tests are executed successfully
// Status BAD REQUEST 				(400): All tests executed, at least on failed or not assessed
// Status NOT FOUND 				(404): Not all tests executed or no tests for the version+variant
// Status FORBIDDEN 				(403): Wrong token
// Status INTERNAL SERVER ERROR 	(500): Error computing the current state
func IntegrationPost(caseLister handler.TestCaseLister, caseAdder handler.TestCaseAdder, protocolLister handler.CaseProtocolLister,
	taskLister handler.TaskListGetter, taskAdder handler.TaskListAdder, projectAdder handler.ProjectAdder) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		contextEntities := handler.GetContextEntities(r)
		if contextEntities.ContainerID == "" || contextEntities.Project == nil {
			errors.Handle(contextEntities.Err, w, r)
			return
		}

		requestBody := CIRequestBody{}
		if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
			errors.Handle(err, w, r)
			return
		}

		if requestBody.Token != contextEntities.Project.Token {
			errors.Handle(errors.Construct(http.StatusForbidden).Finish(), w, r)
			return
		}

		if err := handleAutomaticAssignment(contextEntities, requestBody, taskLister, taskAdder, caseLister, caseAdder, projectAdder); err != nil {
			errors.Handle(errors.Construct(http.StatusInternalServerError).
				WithLog("error in automatic assignment").
				WithCause(err).WithStackTrace(0).
				Finish(), w, r)
			return
		}

		if response := analyseProtocols(w, contextEntities.Project.ID(), caseLister, protocolLister, requestBody); response != nil {
			errors.Handle(response, w, r)
			return
		}
	}
}

// handleAutomaticAssignment handles the automatic assignment of tests for the given version.
//
// Returns an error if any occurs
func handleAutomaticAssignment(contextEntities *handler.ContextEntities, requestBody CIRequestBody,
	taskLister handler.TaskListGetter, taskAdder handler.TaskListAdder, caseLister handler.TestCaseLister,
	caseAdder handler.TestCaseAdder, projectAdder handler.ProjectAdder) error {

	testsAreAssigned, err := testsAreAssigned(taskLister, contextEntities.Project, requestBody)
	if err != nil {
		return err
	}
	if testsAreAssigned {
		// Do nothing and wait for the tests to be executed
		return nil
	}

	if versionExists(contextEntities.Project.Versions, requestBody.Version) {
		// If the version exists but there are no applicable versions,
		// also create a task for the managers to assign this version+variant
		// to the test-cases
		testHaveApplicableVersionAndVariant, err := testHaveApplicableVersionAndVariant(contextEntities, caseLister, requestBody)
		if err != nil {
			return err
		}
		if testHaveApplicableVersionAndVariant {
			managerSetAssignmentTaskDone, err := managerSetAssignmentTaskDone(taskLister, contextEntities.Project, requestBody)
			if err != nil {
				return err
			}
			if managerSetAssignmentTaskDone {
				// In order to create tasks for the testers to test the applicable test-cases,
				// the managers have to set the task as done (this is needed to know that they are not
				// still in the process of setting the applicability) and there have to be
				// test-cases that are applicable to the sut-version+sut-variant (this prevents
				// the case that a lazy manager just sets the assignment-task as done).
				err = createTaskForTesters(taskLister, taskAdder, caseLister, contextEntities, requestBody)
				if err != nil {
					return err
				}
			}
		} else {
			if err = assignVersionToTests(caseLister, caseAdder, contextEntities.Project, requestBody.Version); err != nil {
				return err
			}

			if err = createTaskForManagers(taskLister, taskAdder, contextEntities, requestBody); err != nil {
				return err
			}
		}
	} else {
		if err := createSUTVariant(contextEntities, projectAdder, requestBody); err != nil {
			return err
		}

		if err = assignVersionToTests(caseLister, caseAdder, contextEntities.Project, requestBody.Version); err != nil {
			return err
		}

		if err = createTaskForManagers(taskLister, taskAdder, contextEntities, requestBody); err != nil {
			return err
		}
	}

	return nil
}

// assignVersionToTests assigns the given version to the test-case, if the
// test-case was applicable to the previous version. The previous key is
// currently the version that comes in the map of versions before the given
// version (because versions are not sorted). It is just assumed that the
// versions are alphabetically ordered.
func assignVersionToTests(caseLister handler.TestCaseLister, caseAdder handler.TestCaseAdder, pr *project.Project, version string) error {
	cases, err := caseLister.List(pr.ID())
	if err != nil {
		return err
	}

	previousVersion := getPreviousVersion(pr.Versions, version)

	for _, cs := range cases {
		if previousVersion == nil || !isApplicableTo(cs, previousVersion.Name) {
			continue
		}

		testCaseVersion := cs.TestCaseVersions[0]
		testCaseVersion.Versions[version] = pr.Versions[version]

		commitText := "Assigned version " + version

		newTestCaseVersion := test.NewTestCaseVersion(testCaseVersion.VersionNr+1, false, commitText, testCaseVersion.Description, testCaseVersion.Preconditions,
			testCaseVersion.Versions, testCaseVersion.Duration, testCaseVersion.Case)

		newTestCaseVersion.Steps = testCaseVersion.Steps

		cs.TestCaseVersions = append([]test.CaseVersion{newTestCaseVersion}, cs.TestCaseVersions...)
		if err := caseAdder.Add(cs); err != nil {
			return err
		}
	}

	return nil
}

// getPreviousVersion returns the version that comes before the currentVersion.
// It is needed to check which tests are applicable to the previous version. Those
// tests will be applicable to the currentVersion by default.
//
// Returns the version if there is any. Otherwise returns nil.
// If the currentVersion is not in the map, returns the last version.
func getPreviousVersion(versions map[string]*project.Version, currentVersion string) *project.Version {
	var previousVersion *project.Version = nil

	// Sort alphabetically into an array
	versionsArray := make([]string, len(versions))
	i := 0
	for k := range versions {
		versionsArray[i] = k
		i++
	}
	sort.Strings(versionsArray)

	// Now check which version is alphabetically before
	for _, version := range versionsArray {
		if version == currentVersion {
			break
		}
		previousVersion = versions[version]
	}
	return previousVersion
}

func isApplicableTo(testCase *test.Case, version string) bool {
	if _, contains := testCase.TestCaseVersions[0].Versions[version]; contains {
		return true
	}
	return false
}

// testHaveApplicableVersionAndVariant checks if there is ANY test-case
// that has the version with the variant assigned.
func testHaveApplicableVersionAndVariant(contextEntities *handler.ContextEntities, caseLister handler.TestCaseLister, requestBody CIRequestBody) (bool, error) {
	cases, err := caseLister.List(contextEntities.Project.ID())
	if err != nil {
		return false, err
	}

	for _, cs := range cases {
		if version, ok := cs.TestCaseVersions[0].Versions[requestBody.Version]; ok {
			if variantExists(version.Variants, requestBody.Variant) {
				return true, nil
			}
		}
	}

	return false, nil
}

func createSUTVariant(contextEntities *handler.ContextEntities, projectAdder handler.ProjectAdder, requestBody CIRequestBody) error {
	contextEntities.Project.Versions[requestBody.Version] = &project.Version{
		Name: requestBody.Version,
		Variants: []project.Variant{
			{Name: requestBody.Variant},
		},
	}

	if err := projectAdder.Add(contextEntities.Project); err != nil {
		return err
	}
	return nil
}

// createTaskForManagers creates a task for every manager in the project
// to assign the given version to all appropriate test-cases.
//
// If a manager already has a task, he will not be given another task.
// This helps adding tasks for managers that were given the role after
// the first request from the ci/cd came in.
func createTaskForManagers(taskLister handler.TaskListGetter, taskListAdder handler.TaskListAdder,
	contextEntities *handler.ContextEntities, requestBody CIRequestBody) error {

	for _, memberShip := range contextEntities.Project.UserMembers {
		if memberShip.Role != "Manager" {
			continue
		}

		taskList, err := taskLister.Get(memberShip.User.Actor())
		if err != nil {
			return err
		}

		if managerAlreadyHasTask(taskList, contextEntities.Project.Name, requestBody) {
			continue
		}

		taskList = taskList.AddItem(
			memberShip.User, contextEntities.Project.Owner, contextEntities.Project.ID(),
			task.SUTVersionAssignment, task.SUTVersion, requestBody.Version,
			requestBody.Version, requestBody.Variant, time.Time{})

		err = taskListAdder.Add(*taskList)
		if err != nil {
			return err
		}

	}

	return nil
}

// managerAlreadyHasTask checks if a manager already has a task for the version+variant
// in the requestBody.
//
// Returns true if there is a task in the taskList, otherwise false.
func managerAlreadyHasTask(taskList *task.List, projectName string, requestBody CIRequestBody) bool {
	for _, taskItem := range taskList.Tasks {
		// If the manager already has a task, don't create a new task for this manager
		if taskItem.ProjectID.Project() == projectName &&
			taskItem.Type == task.SUTVersionAssignment && taskItem.Reference.Type == task.SUTVersion &&
			taskItem.Version == requestBody.Version && taskItem.Variant == requestBody.Variant {

			return true
		}
	}
	return false
}

// createTaskForTesters creates a task for every tester in a project
// to test the test-cases that are applicable to the given version.
func createTaskForTesters(taskLister handler.TaskListGetter, taskListAdder handler.TaskListAdder, caseLister handler.TestCaseLister,
	contextEntities *handler.ContextEntities, requestBody CIRequestBody) error {

	proj := contextEntities.Project

	testCases, err := caseLister.List(proj.ID())
	if err != nil {
		return err
	}

	for _, memberShip := range proj.UserMembers {
		if memberShip.Role != "Tester" {
			continue
		}

		taskList, err := taskLister.Get(memberShip.User.Actor())
		if err != nil {
			return err
		}

		for _, testCase := range testCases {
			if versionExists(testCase.TestCaseVersions[0].Versions, requestBody.Version) &&
				!testerAlreadyHasTask(taskList, contextEntities.Project.Name, testCase.Name, requestBody) {

				// The owner is set as author because a reference to an existing user is needed
				taskList = taskList.AddItem(memberShip.User, proj.Owner, proj.ID(),
					task.Assignment, task.Case, testCase.Name,
					requestBody.Version, requestBody.Variant, time.Time{})
			}
		}

		err = taskListAdder.Add(*taskList)
		if err != nil {
			return err
		}

	}

	return nil
}

// testerAlreadyHasTask checks if a tester already has a task for the version+variant
// in the requestBody for a test-case.
//
// Returns true if there is a task in the taskList, otherwise false.
func testerAlreadyHasTask(taskList *task.List, projectName string, caseName string, requestBody CIRequestBody) bool {
	for _, taskItem := range taskList.Tasks {
		if taskItem.ProjectID.Project() == projectName && taskItem.Reference.ID == caseName &&
			taskItem.Type == task.Assignment && taskItem.Reference.Type == task.Case &&
			taskItem.Version == requestBody.Version && taskItem.Variant == requestBody.Variant {
			return true
		}
	}
	return false
}

// testsAreAssigned checks whether the tests
// are already assigned to the testers in a project.
//
// This is done by checking if ANY member with the role tester has a task
// to test AT LEAST ONE test-case. This prevents the case that an unnecessary
// automatic assignment is done and the tests were already assigned.
func testsAreAssigned(taskLister handler.TaskListGetter, project *project.Project, requestBody CIRequestBody) (bool, error) {
	for _, memberShip := range project.UserMembers {
		if memberShip.Role != "Tester" {
			continue
		}

		taskList, err := taskLister.Get(memberShip.User.Actor())
		if err != nil {
			return false, err
		}

		for _, taskItem := range taskList.Tasks {
			if taskItem.ProjectID.Project() == project.Name && taskItem.Reference.Type == task.Case &&
				requestBody.Version == taskItem.Version && requestBody.Variant == taskItem.Variant {
				return true, nil
			}
		}
	}

	return false, nil
}

// managerSetAssignmentTaskDone checks whether the test-managers have
// already assigned the new version to the test-cases.
//
// This is done by checking if ANY manager has set the task for
// assigning the version to the test-cases as done.
//
// If there is no member or manager, return true,
// so the tests can be assigned to the testers.
// If no manager has a task, return true
func managerSetAssignmentTaskDone(taskLister handler.TaskListGetter, project *project.Project, requestBody CIRequestBody) (bool, error) {
	atLeastOneManagerHasTask := false

	for _, memberShip := range project.UserMembers {
		if memberShip.Role != "Manager" {
			continue
		}

		taskList, err := taskLister.Get(memberShip.User.Actor())
		if err != nil {
			return true, err
		}

		for _, taskItem := range taskList.Tasks {
			if taskItem.Reference.Type == task.SUTVersion && taskItem.Version == requestBody.Version &&
				taskItem.Variant == requestBody.Variant {
				atLeastOneManagerHasTask = true
				if taskItem.Done {
					return true, nil
				}
			}
		}
	}

	// If there is a task for at least one manager but it is not done,
	// return false
	if atLeastOneManagerHasTask {
		return false, nil
	}

	return true, nil
}

// versionExists checks if the version exists in the map of versions
func versionExists(versions map[string]*project.Version, version string) bool {
	_, ok := versions[version]
	return ok
}

// variantExists checks if the given variant is in the slice of variants
func variantExists(variants []project.Variant, variant string) bool {
	for _, v := range variants {
		if v.Name == variant {
			return true
		}
	}
	return false
}
