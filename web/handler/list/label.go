/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

// containsLabels checks whether labelSet contains all of labelCheck
// Returns true if labelSet contains all labels in labelCheck
// Else return false.
func containsLabels(labelCheck []project.Label, labelSet *[]*project.Label) bool {

	for _, comparisonLable := range labelCheck {
		var contains = false
		for _, referenceLable := range *labelSet {
			if referenceLable.Name == comparisonLable.Name {
				contains = true
			}
		}
		if !contains {
			return false
		}
	}
	return true

}
