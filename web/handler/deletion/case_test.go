/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseDelete(t *testing.T) {
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
			middleware.ProjectKey:  handler.DummyProject,
		},
	)
	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
			middleware.ProjectKey:  nil,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     nil,
			middleware.ProjectKey:  handler.DummyProject,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.TestCaseKey: handler.DummyTestCaseSUTVersions,
			middleware.ProjectKey:  handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseDeleterMock{}
				return CaseDelete(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No project in  context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseDeleterMock{}
				return CaseDelete(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleFragmentRequest(ctxNoProject, http.MethodGet, handler.NoParams),
			handler.SimpleRequest(ctxNoProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No user in  context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseDeleterMock{}
				return CaseDelete(m), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodGet, handler.NoParams),
			handler.SimpleRequest(ctxNoUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseDeleterMock{}
				return CaseDelete(m), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseDeleterMock{}
				return CaseDelete(m), handler.Matches(
					handler.HasStatus(http.StatusSeeOther),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
