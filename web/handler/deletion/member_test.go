/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestMemberDelete(t *testing.T) {

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    nil,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUserUnauthorized,
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	paramsList := url.Values{}
	paramsList.Add(httputil.Members, "[\"benweiss\"]")

	handler.Suite(t,
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				userRetriever := &handler.UserRetrieverMock{}
				projectAdder := &handler.ProjectAdderMock{}

				return MemberDelete(userRetriever, projectAdder),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodDelete, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodDelete, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				userRetriever := &handler.UserRetrieverMock{}
				projectAdder := &handler.ProjectAdderMock{}

				return MemberDelete(userRetriever, projectAdder),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
					)
			},
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodDelete, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodDelete, handler.NoParams),
		),
		handler.CreateTest("Member is owner",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				userRetriever := &handler.UserRetrieverMock{
					User: &user.User{
						Name: "default",
					},
					UserFound: true,
				}
				projectAdder := &handler.ProjectAdderMock{}

				return MemberDelete(userRetriever, projectAdder),
					handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
					)
			},
			handler.SimpleRequest(ctx, http.MethodDelete, paramsList),
			handler.SimpleFragmentRequest(ctx, http.MethodDelete, paramsList),
		),
	)
}
