/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"github.com/dimfeld/httptreemux"
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/export"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/integration/ci_cd"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

func registerProjectsHandler(authRouter *httptreemux.ContextMux) {
	userStore := store.GetUserStore()
	groupStore := store.GetGroupStore()
	projectStore := store.GetProjectStore()
	commentStore := store.GetCommentStore()

	testCaseStore := store.GetCaseStore()
	testSequenceStore := store.GetSequenceStore()
	protocolStore := store.GetProtocolStore()

	negroniWrap := negroni.New(middleware.Container(userStore, groupStore), middleware.Project(projectStore))

	sessionStore := sessions.GetSessionStore()
	sessionNegroni := negroni.New(middleware.Auth(sessionStore))
	registerProjectCreateHandler(authRouter, sessionNegroni, projectStore, testCaseStore, testSequenceStore)

	projectGroup := wrapContextGroup(authRouter.NewContextGroup(VarContainer + VarProject))

	registerProjectHandler(projectGroup, negroniWrap, projectStore)
	registerCasesHandler(projectGroup, negroniWrap, testCaseStore)
	registerSequencesHandler(projectGroup, negroniWrap, testSequenceStore)
	registerProtocolsHandler(projectGroup, negroniWrap, testCaseStore, testSequenceStore, protocolStore)
	registerMembersHandler(projectGroup, negroniWrap)
	registerDashboardHandler(projectGroup, negroniWrap, testCaseStore, testSequenceStore)
	registerProjectExportHandler(projectGroup, negroniWrap, testCaseStore, testSequenceStore)
	registerCommentsHandler(projectGroup, negroniWrap, commentStore)
	registerCIIntegrationHandler(projectGroup, negroniWrap, testCaseStore, protocolStore, projectStore)
	registerActivityHandler(projectGroup, negroniWrap)
}

func registerProjectCreateHandler(authRouter *httptreemux.ContextMux, nBasic *negroni.Negroni, projectStore store.ProjectsSQL, testCaseStore store.CasesSQL, testSequenceStore store.SequencesSQL) {
	labelStore := store.GetLabelStore()

	authRouter.Handler(http.MethodGet, Projects+New,
		nBasic.With(negroni.WrapFunc(display.CreateProjectGet(projectStore))))
	authRouter.Handler(http.MethodPost, Projects+Save,
		nBasic.With(negroni.WrapFunc(creation.ProjectPost(projectStore, projectStore, testCaseStore, testCaseStore, testCaseStore, testSequenceStore, labelStore))))
}

func registerCIIntegrationHandler(pg *contextGroup, n *negroni.Negroni, caseStore store.CasesSQL,
	protocolStore store.ProtocolsSQL, projectStore store.ProjectsSQL) {

	taskStore := store.GetTaskStore()
	pg.HandlerPost("/ci",
		n.With(negroni.WrapFunc(
			ci_cd.IntegrationPost(caseStore, caseStore, protocolStore, taskStore, taskStore, projectStore))))
}

func registerProjectHandler(pg *contextGroup, n *negroni.Negroni, ps store.ProjectsSQL) {

	pg.HandlerGet(Show,
		n.With(negroni.Wrap(defaultRedirect("dashboard"))))

	registerProjectVersionsHandler(pg, n, ps)
	registerProjectSettingsHandler(pg, n, ps)
	registerProjectLabelsHandler(pg, n)
	registerProjectRolesHandler(pg, n, ps)
}

func registerProjectVersionsHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectSUTVersionUpdater) {
	pg.HandlerGet(Versions,
		n.With(negroni.WrapFunc(json.ProjectVariantsGet)))
	pg.HandlerPost(Versions,
		n.With(negroni.WrapFunc(json.ProjectVariantsPost(ps))))
	pg.HandlerPut(Versions,
		n.With(negroni.WrapFunc(update.ProjectVariantsPut(ps))))
}

func registerProjectSettingsHandler(cg *contextGroup, n *negroni.Negroni, ps store.ProjectsSQL) {

	cg.HandlerGet(SettingsProject,
		n.With(negroni.WrapFunc(display.GetSettingsProjects)))
	cg.HandlerGet(SettingsProject+Async+SettingsNav,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsProjectsNav)))
	cg.HandlerGet(SettingsProject+Async+SettingsTab,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsProjectsTab)))
	cg.HandlerPost(SettingsProject,
		n.With(negroni.WrapFunc(update.ProjectPost(ps, ps))))
	cg.HandlerDelete(SettingsProject,
		n.With(negroni.WrapFunc(deletion.ProjectDelete(ps))))

	cg.HandlerGet(SettingsPermission,
		n.With(negroni.WrapFunc(display.GetSettingsPermissions)))
	cg.HandlerGet(SettingsPermission+Async+SettingsNav,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsPermissionsNav)))
	cg.HandlerGet(SettingsPermission+Async+SettingsTab,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsPermissionsTab)))
	cg.HandlerPut(SettingsPermission+Roles,
		n.With(negroni.WrapFunc(update.ProjectRolesPut(ps))))
	cg.HandlerDelete(SettingsPermission+Roles+Delete,
		n.With(negroni.WrapFunc(deletion.ProjectRoleDelete(ps))))

	cg.HandlerGet(SettingsIntegrations,
		n.With(negroni.WrapFunc(display.GetSettingsIntegrations)))
	cg.HandlerGet(SettingsIntegrations+Async+SettingsNav,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsIntegrationsNav)))
	cg.HandlerGet(SettingsIntegrations+Async+SettingsTab,
		n.With(negroni.WrapFunc(display.GetAsyncSettingsIntegrationsTab)))
	cg.HandlerPut(SettingsIntegrations,
		n.With(negroni.WrapFunc(update.IntegrationsPut(ps))))
}

func registerProjectRolesHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectAdder) {
	pg.HandlerGet(Roles,
		n.With(negroni.WrapFunc(json.ProjectRolesGet)))
}

func registerProjectExportHandler(pg *contextGroup, n *negroni.Negroni, caseStore store.CasesSQL, sequenceStore store.SequencesSQL) {
	labelStore := store.GetLabelStore()

	pg.HandlerPost(SettingsProject+Export, n.With(negroni.WrapFunc(export.ProjectJson(caseStore, sequenceStore, labelStore))))
}

func registerCommentsHandler(pg *contextGroup, n *negroni.Negroni, commentStore handler.Comments) {
	pg.HandlerPut(Comment, n.With(negroni.WrapFunc(update.CommentPut(commentStore))))
}

func registerProjectLabelsHandler(pg *contextGroup, n *negroni.Negroni) {
	labelStore := store.GetLabelStore()

	labelContextGroup := wrapContextGroup(pg.NewContextGroup(Labels))

	labelContextGroup.HandlerGet(Show,
		n.With(negroni.WrapFunc(json.ProjectLabelsGet(labelStore))))
	labelContextGroup.HandlerPost(New,
		n.With(negroni.WrapFunc(creation.ProjectLabelPost(labelStore))))
	labelContextGroup.HandlerPut(Delete,
		n.With(negroni.WrapFunc(deletion.DeleteProjectLabelPut(labelStore))))
}
