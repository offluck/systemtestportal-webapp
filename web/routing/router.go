/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"log"
	"net/url"
	"path"
	"runtime/debug"

	"github.com/dimfeld/httptreemux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/unrolled/secure"
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/monitoring"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

// InitRouter initializes the main router that
// handles all incoming requests
func InitRouter() http.Handler {
	n := initSession()

	authRouter := registerAuthRouter()
	staticRouter := registerStaticRouter()

	registerHeaders(n)
	registerAboutGet(authRouter)
	registerImprintGet(authRouter)
	registerPrivacyGet(authRouter)
	registerLicenseGet(authRouter)
	registerAccountHandler(authRouter)
	registerExploreHandler(authRouter)
	registerSystemSettingsHandler(authRouter)
	registerProjectsHandler(authRouter)
	registerGroupHandler(authRouter)
	registerTaskHandler(authRouter)
	registerUtilHandler(authRouter)
	registerProfileHandler(authRouter)
	authRouter.PathSource = httptreemux.URLPath

	n.UseHandler(authRouter)
	staticRouter.Handler(http.MethodGet, "/", defaultRedirect(Explore))

	// Add an HTTP endpoint to expose metrics with default options
	staticRouter.Handler(http.MethodGet, "/metrics", promhttp.HandlerFor(
		monitoring.Gatherer(),
		promhttp.HandlerOpts{},
	))

	allMethods(staticRouter, "/*", n)
	staticRouter.PathSource = httptreemux.URLPath

	return promhttp.InstrumentHandlerDuration(monitoring.RequestDurationSeconds(), staticRouter)
}

func initSession() *negroni.Negroni {
	sessions.InitSessionManagement(nil, nil)
	sessionStore := sessions.GetSessionStore()
	return negroni.New(middleware.Auth(sessionStore))
}

func registerAuthRouter() *httptreemux.ContextMux {
	r := httptreemux.NewContextMux()
	r.NotFoundHandler = notFound
	r.MethodNotAllowedHandler = methodNotAllowed
	r.PanicHandler = panic

	return r
}

func registerStaticRouter() *httptreemux.ContextMux {
	rs := httptreemux.NewContextMux()
	rs.NotFoundHandler = notFound
	rs.MethodNotAllowedHandler = methodNotAllowed
	rs.PanicHandler = panic
	rs.Handler(http.MethodGet, "/static/*", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	rs.Handler(http.MethodGet, "/favicon.ico", http.FileServer(http.Dir("static/img/favicons/")))
	rs.Handler(http.MethodGet, path.Join(config.Get().DataDir, "projects", "*"), http.StripPrefix(path.Join(config.Get().DataDir, "projects"), http.FileServer(http.Dir(path.Join(config.Get().DataDir, "projects")))))
	rs.Handler(http.MethodGet, path.Join(config.Get().DataDir, "users", "*"), http.StripPrefix(path.Join(config.Get().DataDir, "users"), http.FileServer(http.Dir(path.Join(config.Get().DataDir, "users")))))

	return rs
}

const (
	issueTracker = "<a href='" +
		"https://gitlab.com/stp-team/systemtestportal-webapp/issues'>issue tracker" +
		"</a>"
	errPanicTitle = "An unexpected error occurred."
	errPanic      = "It seems your request couldn't be handled correctly. " +
		"This is most likely a bug. If you want  " +
		"please contact us via our " + issueTracker + "."
)

// panic is used to handle panic while routing.
func panic(w http.ResponseWriter, r *http.Request, v interface{}) {
	if err, ok := v.(error); ok {
		log.Print("Recovered from panic with following error:")
		errors.Handle(err, w, r)
	} else {
		log.Printf("Recovered from panic with following content:\n%v", v)
		errors.ConstructStd(http.StatusInternalServerError,
			errPanicTitle, errPanic, r).
			Respond(w)
	}
	debug.PrintStack()
}

// redirect returns a simple handler that just
// redirects to given url using given status.
func redirect(redirectURL string, status int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//This workaround is necessary, because otherwise the url-package will complete the relative url to an absolute.
		//And will also encode the path, but not enough chars. Therefore we do this manually and use RawPath.
		newURL := redirectURL
		if u, err := url.Parse(newURL); err == nil &&
			u.Scheme == "" &&
			u.Host == "" &&
			(newURL == "" || newURL[0] != '/') {
			oldPath := r.URL.EscapedPath()
			if oldPath == "" {
				oldPath = "/"
			}
			oldDir, _ := path.Split(oldPath)
			newURL = oldDir + newURL
		}
		http.Redirect(w, r, newURL, status)
	}
}

// defaultRedirect returns a simple handler that just
// redirects to given url using 303 as status.
func defaultRedirect(url string) http.HandlerFunc {
	return redirect(url, http.StatusSeeOther)
}

const (
	errNotFoundTitle = "The page you requested doesn't exist."
	errNotFound      = "We are sorry, but we were unable to resolve the page " +
		"you requested. If you believe this is a bug " +
		"please contact us via our " + issueTracker + "."
	errMethodNotAllowedTitle = "Method not allowed."
	errMethodNotAllowed      = "It seems the request you send used an " +
		"unsupported method. If you believe this is a bug " +
		"please contact us via our " + issueTracker + "."
)

// notFound responds with a not found page on every request.
func notFound(w http.ResponseWriter, r *http.Request) {
	errors.ConstructStd(http.StatusNotFound,
		errNotFoundTitle, errNotFound, r).
		Respond(w)
}

// methodNotAllowed responds with a method not allowed page on every request.
//noinspection GoUnusedParameter
func methodNotAllowed(w http.ResponseWriter, r *http.Request,
	methods map[string]httptreemux.HandlerFunc) {
	errors.ConstructStd(http.StatusMethodNotAllowed,
		errMethodNotAllowedTitle, errMethodNotAllowed, r).
		Respond(w)
}

func registerHeaders(n *negroni.Negroni) {
	secureMiddleware := secure.New(secure.Options{
		SSLRedirect:          config.Get().SSLRedirect,                        // If SSLRedirect is set to true, then only allow HTTPS requests. Default is false.
		SSLTemporaryRedirect: config.Get().SSLTemporaryRedirect,               // If SSLTemporaryRedirect is true, the a 302 will be used while redirecting. Default is false (301).
		SSLProxyHeaders:      map[string]string{"X-Forwarded-Proto": "https"}, // SSLProxyHeaders is set of header keys with associated values that would indicate a valid HTTPS request. Useful when using Nginx: `map[string]string{"X-Forwarded-Proto": "https"}`. Default is blank map.
		STSSeconds:           config.Get().STSeconds,                          // STSSeconds is the max-age of the Strict-Transport-Security header. Default is 0, which would NOT include the header.
		STSIncludeSubdomains: true,                                            // If STSIncludeSubdomains is set to true, the `includeSubdomains` will be appended to the Strict-Transport-Security header. Default is false.
		STSPreload:           config.Get().STSPreload,                         // If STSPreload is set to true, the `preload` flag will be appended to the Strict-Transport-Security header. Default is false.
		ForceSTSHeader:       config.Get().ForceSTSHeader,                     // STS header is only included when the connection is HTTPS. If you want to force it to always be added, set to true. `IsDevelopment` still overrides this. Default is false.
		FrameDeny:            config.Get().FrameDeny,                          // If FrameDeny is set to true, adds the X-Frame-Options header with the value of `DENY`. Default is false.
		ContentTypeNosniff:   config.Get().ContentTypeNosniff,                 // If ContentTypeNosniff is true, adds the X-Content-Type-Options header with the value `nosniff`. Default is false.
		BrowserXssFilter:     config.Get().BrowserXssFilter,                   // If BrowserXssFilter is true, adds the X-XSS-Protection header with the value `1; mode=block`. Default is false.
		ReferrerPolicy:       config.Get().ReferrerPolicy,                     // ReferrerPolicy allows the Referrer-Policy header with the value to be set with a custom value. Default is "".
		FeaturePolicy:        config.Get().FeaturePolicy,                      // FeaturePolicy allows the Feature-Policy header with the value to be set with a custom value. Default is "".

		ContentSecurityPolicy: config.Get().CSP,
		IsDevelopment:         false, // This will cause the AllowedHosts, SSLRedirect, and STSSeconds/STSIncludeSubdomains options to be ignored during development. When deploying to production, be sure to set this to false.
	})

	n.Use(negroni.HandlerFunc(secureMiddleware.HandlerFuncWithNext))

}
