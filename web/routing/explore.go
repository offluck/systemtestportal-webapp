/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/dimfeld/httptreemux"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
)

func registerExploreHandler(r *httptreemux.ContextMux) {
	projectStore := store.GetProjectStore()
	groupStore := store.GetGroupStore()
	userStore := store.GetUserStore()
	testcaseStore := store.GetCaseStore()
	protocolStore := store.GetProtocolStore()


	eg := r.NewGroup(Explore)

	eg.GET("/", defaultRedirect("projects"))
	eg.GET(Projects, list.ProjectsGet(projectStore))
	eg.GET(Users, list.UsersGet(userStore))
	eg.GET(Groups, list.GroupsGet(groupStore))
	eg.GET(GlobalDashboard, list.GlobalDashboardGet(projectStore,testcaseStore,protocolStore))
	eg.GET("/*", defaultRedirect("projects"))
}
