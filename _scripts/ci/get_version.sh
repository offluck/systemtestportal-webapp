#!/usr/bin/env bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#
REVISION=1
if [[ $(git describe --tags --abbrev=0) =~ .*[0-9].* ]]; then
    # If latest tag contains number cut everything before it and use it
    # as version number.
    VERSION=$(git describe --tags | sed 's/^[^0-9]*\(.*\)$/\1/')
else
    # Otherwise append 0 to be compatible with debains restriction
    # on version numbers.
    VERSION=0-$(git describe --tags)
fi
# Remove suite marker
if [[ ${VERSION##*-} =~ (testing|stable|unstable) ]]; then
    VERSION=${VERSION%:-}
fi
# Remove invalid symbols
VERSION=${VERSION//[^-A-Za-z0-9+~.]/}
APPENDIX=${1//[^-A-Za-z0-9+~.]/}
if [ -n "$APPENDIX" ]; then
    VERSION="${VERSION}~$APPENDIX"
fi
echo ${VERSION}-${REVISION}