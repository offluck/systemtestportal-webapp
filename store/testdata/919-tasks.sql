-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO todo_lists (id, user_id)
VALUES
    (1, 1),
    (2, 2),
    (3, 3);

INSERT INTO todo_items (id, list_id, todo_item_index, author_id, project_id, reference_id, reference_type, deadline, creation_date, done, type)
VALUES 
    (1, 1, 1, 1, 2, 2, 0, '0001-01-01 00:00:00', '0001-01-01 00:00:00', 0, 0),
    (2, 1, 2, 1, 2, 3, 0, '0001-01-01 00:00:00', '0001-01-01 00:00:00', 0, 0),
    (3, 2, 1, 1, 2, 2, 0, '0001-01-01 00:00:00', '0001-01-01 00:00:00', 0, 0),
    (4, 2, 2, 1, 2, 3, 0, '0001-01-01 00:00:00', '0001-01-01 00:00:00', 1, 0),
    (5, 1, 3, 1, 2, 1, 1, '0001-01-01 00:00:00', '0001-01-01 00:00:00', 0, 0),
    (6, 1, 4, 1, 2, 1, 1, '0001-01-01 00:00:00', '0001-01-01 00:00:00', 1, 0);
    
-- +migrate Down
