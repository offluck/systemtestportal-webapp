// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

const (
	errorNoSequence = "no test sequence with name %#v"
)

type SequencesSQL struct {
	e *xorm.Engine
}

type sequenceRow struct {
	Id        int64
	CreatedAt 	time.Time `xorm:"created"`

	// Foreign Key ----------------|
	ProjectID int64
	// ----------------------------|

	Name      string
}

func sequenceFromRow(r sequenceRow) *test.Sequence {
	return &test.Sequence{
		Id:        r.Id,
		CreatedAt: r.CreatedAt,
		ProjectId: r.ProjectID,
		Name:      r.Name,
	}
}

func rowFromSequence(ts *test.Sequence) sequenceRow {
	return sequenceRow{
		Id:        ts.Id,
		CreatedAt: ts.CreatedAt,
		ProjectID: ts.ProjectId,
		Name:      ts.Name,
	}
}

func (s SequencesSQL) List(pID id.ProjectID) ([]*test.Sequence, error) {
	session := s.e.NewSession()
	defer session.Close()

	err := session.Begin()
	if err != nil {
		return nil, err
	}

	tss, err := listSequences(session, pID)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return nil, err
	}

	err = session.Commit()
	if err != nil {
		return nil, err
	}

	return tss, nil
}

func (s SequencesSQL) Add(ts *test.Sequence) error {
	session := s.e.NewSession()
	defer session.Close()

	err := session.Begin()
	if err != nil {
		return err
	}

	err = saveSequence(session, ts)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	return session.Commit()
}

func (s SequencesSQL) Rename(old, new id.TestID) error {
	session := s.e.NewSession()
	defer session.Close()

	err := session.Begin()
	if err != nil {
		return err
	}

	err = renameSequence(session, old, new)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	return session.Commit()
}

func (s SequencesSQL) Get(tID id.TestID) (*test.Sequence, bool, error) {
	session := s.e.NewSession()
	defer session.Close()

	err := session.Begin()
	if err != nil {
		return nil, false, err
	}

	tc, ex, err := getSequence(session, tID)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return nil, false, err
	}

	err = session.Commit()
	if err != nil {
		return nil, false, err
	}

	return tc, ex, nil
}

func (s SequencesSQL) Delete(testSequence *test.Sequence) error {
	testSequenceId := testSequence.ID()
	session := s.e.NewSession()
	defer session.Close()

	err := session.Begin()
	if err != nil {
		return err
	}

	err = deleteSequence(session, testSequenceId)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	err = session.Commit()
	if err != nil {
		return err
	}

	err = GetCommentStore().DeleteCommentsForTest(*testSequence)
	if err != nil {
		return err
	}

	return nil
}

func (s SequencesSQL) Exists(tID id.TestID) (bool, error) {
	return existsSequenceRow(s.e, tID)
}

func saveSequence(s xorm.Interface, ts *test.Sequence) error {
	prID, err := lookupProjectRowID(s, ts.Project)
	if err != nil {
		return err
	}

	osr, ex, err := getSequenceRow(s, prID, ts.Name)
	if err != nil {
		return err
	}

	nsr := rowFromSequence(ts)
	nsr.ProjectID = prID
	if !ex {
		nsr = sequenceRow{
			ProjectID: prID,
			Name: ts.Name,
		}
		err = insertSequenceRow(s, &nsr)

		// Set the rows Id to the sequence for usage
		ts.Id = nsr.Id
	} else {
		nsr.Id = osr.Id
		err = updateSequenceRow(s, &nsr)
	}

	if err != nil {
		return err
	}

	return saveSequenceVersions(s, nsr.Id, ts.SequenceVersions)
}

func renameSequence(s xorm.Interface, old, new id.TestID) error {
	if old.ProjectID != new.ProjectID {
		return errors.New(`unsupported operation: test sequences currently can not be moved between projects`)
	}

	prID, err := lookupProjectRowID(s, old.ProjectID)
	if err != nil {
		return err
	}

	ocr, ex, err := getSequenceRow(s, prID, old.Test())
	if err != nil {
		return err
	}

	if !ex {
		return fmt.Errorf(errorNoCase, old.Test())
	}

	ncr := sequenceRow{Id: ocr.Id, ProjectID: ocr.ProjectID, Name: new.Test()}
	return updateSequenceRow(s, &ncr)
}

func insertSequenceRow(s xorm.Interface, sr *sequenceRow) error {
	_, err := s.Table(sequenceTable).Insert(sr)
	return err
}

func updateSequenceRow(s xorm.Interface, sr *sequenceRow) error {
	aff, err := s.Table(sequenceTable).ID(sr.Id).Update(sr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteSequence(s xorm.Interface, tID id.TestID) error {
	prID, err := lookupProjectRowID(s, tID.ProjectID)
	if err != nil {
		return err
	}

	id, ex, err := getSequenceRowID(s, tID)
	if err != nil {
		return err
	}
	if !ex {
		return nil
	}
	err = deleteTasks(s, 1, id)
	if err != nil {
		return err
	}

	sr := sequenceRow{ProjectID: prID, Name: tID.Test()}
	aff, err := s.Table(sequenceTable).Delete(&sr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoSequence, tID.Test())
	}

	return nil
}

func listSequences(s xorm.Interface, pID id.ProjectID) ([]*test.Sequence, error) {
	prID, err := lookupProjectRowID(s, pID)
	if err != nil {
		return nil, err
	}

	srs, err := listSequenceRows(s, prID)
	if err != nil {
		return nil, err
	}

	var tss []*test.Sequence
	for _, r := range srs {
		var ts *test.Sequence
		ts, err = buildSequence(s, pID, r)

		if err != nil {
			return nil, err
		}

		tss = append(tss, ts)
	}

	sortSequences(tss)

	return tss, nil

}

func getSequence(s xorm.Interface, tID id.TestID) (*test.Sequence, bool, error) {
	prID, err := lookupProjectRowID(s, tID.ProjectID)
	if err != nil {
		return nil, false, err
	}

	sr, ex, err := getSequenceRow(s, prID, tID.Test())
	if err != nil || !ex {
		return nil, false, err
	}

	ts, err := buildSequence(s, tID.ProjectID, sr)
	if err != nil {
		return nil, false, err
	}

	return ts, true, nil
}

func buildSequence(s xorm.Interface, pID id.ProjectID, sr sequenceRow) (*test.Sequence, error) {
	var err error

	labelStore := GetLabelStore()

	ts := sequenceFromRow(sr)
	ts.Project = pID
	ts.SequenceVersions, err = listSequenceVersions(s, ts.Project, sr.Id)
	if err != nil {
		return nil, err
	}

	for i := range ts.SequenceVersions {
		ts.SequenceVersions[i].Testsequence = ts.ID()
	}

	ts.Labels, err = labelStore.GetLabelsForTest(*ts, ts.ProjectId)
	if err != nil {
		return nil, err
	}

	return ts, nil
}

func existsSequenceRow(s xorm.Interface, tID id.TestID) (bool, error) {
	prID, err := lookupProjectRowID(s, tID.ProjectID)
	if err != nil {
		return false, err
	}

	_, ex, err := getSequenceRow(s, prID, tID.Test())
	return ex, err
}

func listSequenceRows(s xorm.Interface, prID int64) ([]sequenceRow, error) {
	var srs []sequenceRow
	err := s.Table(sequenceTable).Asc(nameField).Find(&srs, &sequenceRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}

	return srs, nil
}

func getSequenceRow(s xorm.Interface, prID int64, name string) (sequenceRow, bool, error) {
	sr := sequenceRow{ProjectID: prID, Name: name}
	ex, err := s.Table(sequenceTable).Get(&sr)
	if err != nil {
		return sequenceRow{}, false, err
	}

	if !ex {
		return sequenceRow{}, false, nil
	}

	return sr, true, nil
}

func getSequenceRowID(s xorm.Interface, testID id.TestID) (int64, bool, error) {
	row, ex, err := getSequenceRowByTestID(s, testID)
	return row.Id, ex, err
}

func getSequenceRowByTestID(s xorm.Interface, tID id.TestID) (sequenceRow, bool, error) {
	prID, err := lookupProjectRowID(s, tID.ProjectID)
	if err != nil {
		return sequenceRow{}, false, err
	}

	cr, ex, err := getSequenceRow(s, prID, tID.Test())
	return cr, ex, err
}

func getSequenceRowByID(s xorm.Interface, id int64) (sequenceRow, bool, error) {
	sr := sequenceRow{}
	ex, err := s.Table(sequenceTable).ID(id).Get(&sr)

	if err != nil || !ex {
		return sequenceRow{}, false, err
	}

	return sr, true, nil
}

func sortSequences(tss []*test.Sequence) {
	// sort the projects
	sort.Slice(tss, func(i, j int) bool {
		cmp := strings.Compare(tss[i].Name, tss[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}
