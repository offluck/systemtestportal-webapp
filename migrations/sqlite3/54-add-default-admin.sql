-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO owners VALUES (
    1,
    'admin'
);

INSERT INTO users VALUES (
    1,
    'admin',
    'admin',
    'admin@systemtestportal.org',
    '$2a$10$iravD8A8Sifmp.ErYrhW5udeIx4u0PZvCV.bDtBaLW2MPm9BjbCje',
    '2018-10-04 12:00:00',
    1,
    NULL,
    1,
    0,
    NULL,
    1
);

-- +migrate Down
